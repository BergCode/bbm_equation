currFileName = 'errorEstimatesDet.m';
% [fList,pList] = matlab.codetools.requiredFilesAndProducts('errorEstimates.m');
% folder = fileparts(which(mfilename)); 
% addpath(genpath(folder));
% Modified to use symmetric exponential integrator as reference scheme

%% !! Experiment Specs - Identifies unique experiment
% Time info
T = 1; % Time horizon
NVec = 2.^[9:15,18]; % Number of time steps

% Spatial info
M = 2^12; % Number of spatial points / Fourier modes
L = 20*pi; % Interval radius
XInt = [-L,L]; % Interval
dx = (XInt(2)-XInt(1))/M; % Spatial grid step size
x = XInt(1) + dx*(0:M-1); % Spatial grid
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2, -M/2+1:-1]; % Fourier modes
kSq = absSq(k);

% Misc. info
IV = @(x) 1.3*exp(-2*x.^2); % Initial value
p = 1; % Nonlinearity power
backupName = 'Data/Deterministic/normEstimatesDet2023Aug_'; % Data file name

% Scheme information
schemeNames = {'SE','LT','ST','EE','BE','ME','MP','CN'};
numSchemes = length(schemeNames);
[colMat,schemeMarkers] = SchemePlotInfo(schemeNames);

% L2, H1, LInf
normNames = {'L2','H1','LInf','L2','H1','LInf','Mass','Energy'};
logScaleNorms = [true,true,true,false,false,false,false,false];
energy = @(x) trapz(x.^3+3*x.^2)*dx;
normFuns = {@(x,y) sqrt(L2norm(x-y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(x-y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(x)-ifft(y))),...
    @(x,y) sqrt(L2norm(y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(y))),...
    @(x,y) trapz(wrapIfft(y))*dx,... 
    @(x,y) energy(wrapIfft(y))};

% !! End of experiment specs
%% Vary the additional terms and run a set of experiments for each variant
alpha_vec = [-1 0 1]; beta_vec = [-1 0 1]; gamma_vec = [-1 0 1]; 
num_alpha = length(alpha_vec); num_beta = length(beta_vec); num_gamma = length(gamma_vec);
% Append to reflect change in s and r
fileNameAppendages = {'alphaNeg','alphaNull','alphaPos';
    'betaNeg','betaNull','betaPos';['' ...
    'gammaNeg'],'gammaNull','gammaPos'}; 

[temp1,temp2,temp3] = ndgrid(alpha_vec,beta_vec,gamma_vec); settings = [temp1(:),temp2(:),temp3(:)]; 
[temp1,temp2,temp3] = ndgrid(1:num_alpha,1:num_beta,1:num_gamma); settingIndexes = [temp1(:),temp2(:),temp3(:)];
num_settings = size(settingIndexes,1);

u0 = fft(IV(x));
parfor setting = 1:num_settings
    % Extract parameters
    alpha = alpha_vec(settingIndexes(setting,1));
    beta = beta_vec(settingIndexes(setting,2));
    gamma = gamma_vec(settingIndexes(setting,3));
    % Perform mode-specific pre-calculations
    A = -1i*k./(1+kSq);
    lin_prop = (alpha*kSq-gamma*1i*k+beta)./(1+kSq);
    schemes = {...
        @(currU,h,dW) DetSExp(currU,A,lin_prop,h,p),...
        @(currU,h,dW) DetLTSpl(currU,A,lin_prop,h,p),...
        @(currU,h,dW) DetStrangSpl(currU,A,lin_prop,h,p),...
        @(currU,h,dW) DetExplInt(currU,A,lin_prop,h,p),...
        @(currU,h,dW) DetThetaExp(currU,A,lin_prop,h,p,0),...
        @(currU,h,dW) DetThetaExp(currU,A,lin_prop,h,p,0.5),...
        @(currU,h,dW) DetMPEul(currU,k,kSq,h,p,alpha,gamma,beta),...
        @(currU,h,dW) DetCNEul(currU,k,kSq,h,p,alpha,gamma,beta)};
    % Modify file name
    currBackupName = [backupName ...
        fileNameAppendages{1,settingIndexes(setting,1)} ...
        fileNameAppendages{2,settingIndexes(setting,2)} ...
        fileNameAppendages{3,settingIndexes(setting,3)} ];

    % Samples are saved, so no need to store them for now
    ErrorCalc(NVec,T,currBackupName,u0,1,schemes,...
        normFuns,false,0,currFileName);
end

%% Load available samples and extract, H1 norm drift and convergences
hVec = T./NVec;

numLogPlots = sum(logScaleNorms);
numNorms = length(normFuns);
numN = length(NVec)-1;

% Assuming that it's the first ones that are log scale plots
schemeErrors = zeros(numN,numSchemes,numLogPlots,num_settings);
conservedIndexes = [5,7,8];
conservedNames = {'H1','Mass','Energy'};
numConserves = length(conservedIndexes);
drifts = zeros(numN,numSchemes,num_settings,numConserves); % Extra dimension for later

% Load each smaller saple set and load into memory
for setting = 1:num_settings
    load([backupName ...
        fileNameAppendages{1,settingIndexes(setting,1)} ...
        fileNameAppendages{2,settingIndexes(setting,2)} ...
        fileNameAppendages{3,settingIndexes(setting,3)} ]); % Stored in variable

    for n = 1:numN
        for i = 1:numSchemes
            % Load all the maximum errors
            for norm = 1:numLogPlots
                schemeErrors(n,i,norm,setting) = ...
                    max(variable.normStorage{1,n}(:,i,norm)); 
            end
            % Load the potentially conserved quantities
            for j = 1:numConserves
                drifts(n,i,setting,j) = ...
                    max(abs(variable.normStorage{1,n}(:,i,conservedIndexes(j))...
                    -variable.normStorage{1,n}(1,i,conservedIndexes(j))));
            end
        end
    end
    fprintf(['\nLoaded error ' num2str(setting) ' of ' num2str(num_settings)])
end
fprintf('\n')
clear variable

%% Quick check & aiming for uniform plot colors and markers
for setting = 1:num_settings
    for norm = 1:numLogPlots
        figure(norm)
        clf
        hold on
        for alpha = 1:numSchemes
            plot(hVec(1:end-1),schemeErrors(:,alpha,norm,setting),...
                ['-' schemeMarkers{alpha}],'Color',colMat(alpha,:),'MarkerSize',20,'LineWidth',2.5)
            set(gca, 'XScale', 'log')
            set(gca, 'YScale', 'log')
        end
        hold off
        legend(schemeNames)
        title(['s=' num2str(alpha_vec(settingIndexes(setting,1))),...
            '. r=' num2str(beta_vec(settingIndexes(setting,2))),...
            '. q=' num2str(gamma_vec(settingIndexes(setting,3)))])
    end
    pause(2)
end
%% Conserved quantities
% Mass plotted one for each gamma (9 plots in one is too narrow)
plotMassNames = {'Plots/Deterministic/MassGammaNeg',...
    'Plots/Deterministic/MassGammaNull',...
    'Plots/Deterministic/MassGammaPos'};
% H1 norm and energy plotted in same plot
plotH1EnergyName = 'Plots/Deterministic/Drifts';

% Disable title
noTitle = cell(numLogPlots,1);
for i = 1:numLogPlots
    noTitle{i} = [];
end
% Move legend
legPos = 'northeast';

% OBS! Ugly fix. Y label intruding on first plots title
smallSkips = {'\hphantom{skipss}' '\hphantom{skip}' '\hphantom{skip}'};
% Plot mass conservation first
massIndexes = [4:6;13:15;22:24];
for i = 1:3
    figure(i)
    plotStrongErrors(squeeze(drifts(:,:,massIndexes(i,:),2)),hVec,schemeMarkers, ...
        colMat,schemeNames,normNames,[],[],'Mass drift',noTitle,legPos)
end
% OBS! Ugly fix: Padding inconsistent on left hand side
set(figure(3).Children,'Position',figure(2).Children.Position)
set(figure(1).Children,'Position',figure(3).Children.Position)
set(figure(2).Children,'Position',figure(3).Children.Position)
% Finally, save figures
for i = 1:3
    figure(i)
    pause(1)
    printToPDF(gcf,plotMassNames{i},false) % Don't move to plot folder
    pause(1)
end
%% Plot H1 (momentum) and energy drifts
H1PresIndexes = [5,14,23]; % Ascending order
numPresH1En = length(H1PresIndexes);
energyPresIndexes = 23;
H1EnergyDrift = cat(3,squeeze(drifts(:,:,H1PresIndexes,1)),...
    squeeze(drifts(:,:,energyPresIndexes,3)));
hPows = [1,2];
lineChoice = [2,8];

% Disable title
noTitle = cell(numPresH1En+1,1);
for i = 1:(numPresH1En+1)
    noTitle{i} = [];
end
% Move legend
legPos = 'southeast';

figure(4)
plotStrongErrors(H1EnergyDrift,hVec,schemeMarkers, ...
    colMat,schemeNames,normNames,hPows,lineChoice,'Drift',noTitle,legPos)
% plotStrongErrors(H1EnergyDrift,hVec,schemeMarkers, ...
%     colMat,schemeNames,normNames,hPows,lineChoice,'Maximum drift',presTitles)
% Save figure
pause(1)
% printToPDF(gcf,plotH1EnergyName,false) % Don't move to plot folder
pause(1)
%% Strong convergence
plotName = 'Plots/Deterministic/Conv';
hPows = [1,2];
lineChoice = [3,1];

% Disable title
noTitle = cell(numLogPlots,1);
for i = 1:numLogPlots
    noTitle{i} = [];
end
% Move legend
legPos = 'southeast';

for setting = 1:num_settings
    figure(setting)
    plotStrongErrors(schemeErrors(:,:,:,setting),hVec,schemeMarkers, ...
        colMat,schemeNames,normNames,hPows,lineChoice,[],noTitle,legPos)
    % Save figure
    pause(0.5)
    printToPDF(gcf,[plotName...
        fileNameAppendages{1,settingIndexes(setting,1)} ...
        fileNameAppendages{2,settingIndexes(setting,2)} ...
        fileNameAppendages{3,settingIndexes(setting,3)}],...
        false) % Don't move to plot folder
end