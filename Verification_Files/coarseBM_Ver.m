refN = 2^10;
NVec = 2.^[3,4];
NVec = [NVec,refN];
tVec = linspace(0,1,refN+1);
refW = randn(refN,1)*sqrt(1/refN);
retBMs = coarseBM(refW,NVec);
figure
hold on
plot(tVec,[0;cumsum(refW)])
plot(linspace(0,1,NVec(1)+1),[0;cumsum(retBMs{1})])
plot(linspace(0,1,NVec(2)+1),[0;cumsum(retBMs{2})])


% Visual verification ok.