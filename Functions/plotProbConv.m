function plotProbConv(schemeErrors,hVec,powVec,colMat,normNames, ...
    schemeNames,schemeMarkers,uniformAxes,legPos)
batchSize = size(schemeErrors,1);
numN = size(schemeErrors,2);
numSchemes = size(schemeErrors,3);
numLogPlots = size(schemeErrors,4);

[~,desiredMarkerSize,desiredLineWidth] = AMSFontSize;

% Assumes data loaded as normal
% powVec = linspace(0.9,1.1,101);
numPow = length(powVec);
CVec = zeros(numPow,numN,numSchemes,numLogPlots,batchSize);
propWidth = 1/(batchSize-1);
intWidthIntegral = zeros(numPow,numSchemes,numLogPlots);
for sch = 1:numSchemes
    for norm = 1:numLogPlots
        for i = 1:numPow
            for n = 1:numN
                % Estimate C(epsilon)
                sortedDiff = sort(schemeErrors(:,n,sch,norm),'descend');
                dtPowVec = hVec(n).^powVec;
                CVec(i,n,sch,norm,:) =  sortedDiff/dtPowVec(i);
            end
        end
        % We need to norm the C values
        maxC = squeeze(max(CVec(:,:,sch,norm,:),[],2));
        minC = squeeze(min(CVec(:,:,sch,norm,:),[],2));
        intWidth = (maxC-minC);
%         intWidth = (maxC-minC)./maxC(:,1); % Norm interval width
        intWidthIntegral(:,sch,norm) = sum(intWidth,2)/batchSize;
        % Calculate the integral - rectangular approximation
%         intWidthIntegral(:,sch,norm) = sum(intWidth,2)*propWidth;
        % Calculate the integral - trapezoidal approximation
%         intWidthIntegral(:,sch,norm) = sum(intWidth(:,1:end-1)+intWidth(:,2:end),2)*propWidth/2;
    end
end

% Uniform axes across plot
if uniformAxes
    axisLim = [min(intWidthIntegral(:)),max(intWidthIntegral(:))];
end

%% Plot
% Plot with sparse markers
numMarkers = 11;
markerIndexes = floor(linspace(1,length(powVec),numMarkers));
figure
    [figWidth,figHeight] = AMSFormatScreenSize;
    figPos = [0,0,figWidth,figHeight];
    set(gcf,'Units','Inches','Position',figPos);

tiledlayout(1,numLogPlots,"TileSpacing","tight","Padding","tight")
for i = 1:numLogPlots
    nexttile
    hold on

    % Dummy line
    for s = 1:numSchemes
        plot([-1, -2],[-1 -2],['-' schemeMarkers{s}],'Color',colMat(s,:),...
            'MarkerSize',desiredMarkerSize,'LineWidth',desiredLineWidth)
    end

    % Sparse markers
    for s = 1:numSchemes
        plot(powVec(markerIndexes),intWidthIntegral(markerIndexes,s,i),...
            schemeMarkers{s},'Color',colMat(s,:),...
            'MarkerSize',desiredMarkerSize,'LineWidth',desiredLineWidth)
    end

    % Line
    for s = 1:numSchemes
        plot(powVec,intWidthIntegral(:,s,i),'Color',colMat(s,:),...
            'MarkerSize',desiredMarkerSize,'LineWidth',desiredLineWidth)
    end

    % X-axis
    xlim([min(powVec),max(powVec)])
    xlabel('$\delta$','Interpreter','latex')

    % Y-axis
    if uniformAxes
        ylim(axisLim)
        if i == 1
            ylabel('$I(\delta)$','Interpreter','latex')
%         else
%             set(gca,'YTickLabel',[])
%         end
    end
    set(gca, 'YScale', 'log')

    % Additional text
    title(normNames{i})
    if i == 1
        legend(schemeNames{:},'Location',legPos)
    end
    set(gca,'FontSize',AMSFontSize)
end
end