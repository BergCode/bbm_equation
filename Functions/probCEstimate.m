function [cVec] = probCEstimate(errorMat,deltaVec,hVec)
% errorMat is of dimension 2, with sample ID and time step size ID
% deltaVec is a vector, which determines the size of the c estimate vector
% hVec is a vector containing the step sizes
cVec = zeros(size(deltaVec));
numN = length(hVec);
% We loop over the estimates of C
% If C yields a P mean of > 0.85, increase C
% If C yields a P mean of < 0.15, decrease C
pLow = 0.15;
pHigh = 0.85;
for i = 1:length(cVec)
    pMean = 0;
    % Track how many times we have switched between increasing or
    % decreasing the C estimate. If we switch too many times, decrease the
    % C increment size
    numSwitches = 0;
    CIncrementSize = 1;
    CIncreasing = false; % Start with decreasing
    while pMean < pLow || pMean > pHigh
        % Estimate the p-values
        pVec = zeros(numN,1);
        for n = 1:numN
            pVec(n) = mean(...
                errorMat(:,n) > (10^cVec(i))*(hVec(n).^deltaVec(i))...
            );
        end
        pMean = mean(pVec);

        % Check if we need finer increments
        if numSwitches > 2
            numSwitches = 0;
            CIncrementSize = CIncrementSize/10;
        end
        
        % If we want to decrease C
        if pMean < pLow
            if CIncreasing
                CIncreasing = false;
                numSwitches = numSwitches + 1;
            end
            cVec(i) = cVec(i) - CIncrementSize;
        end
        
        % If we want to increase C
        if pMean > pHigh
            if ~CIncreasing
                CIncreasing = true;
                numSwitches = numSwitches + 1;
            end
            cVec(i) = cVec(i) + CIncrementSize;
        end
    end
end
end

