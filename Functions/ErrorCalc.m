function ErrorCalc(NVec,T,backupName,u0,batchSize,schemes,normFuns,...
    normOfRef,sampleOffset,scriptName)
% ErrorCalc - Tracks the evolution of a number of norms given a number of
%             schemes. Results are saved along with the experiments
%             specifics (specs) found delineated using '!!' in the
%             scriptCalling file.
% Syntax: ErrorCalc(NVec,T,backupName,u0,batchSize,schemes,normFuns,...
%                   normOfRef,sampleOffset,scriptName)
% Input:
% NVec         - An integer vector defining time discretization, assuming
%                increasing increments. The last index will be used for the
%                reference solution, and will be assumed to be divisible by
%                all other N values.
% T            - The time horizon for the evolution.
% backupName   - The file name used for the data and specs. Specs will be
%                saved as [backupName 'Specs.mat'].
% u0           - Initial value for the samples.
% batchSize    - Positive integer defining how many samples are sought.
%                Will check (and potentially load) already present samples 
%                based on backupName.
% schemes      - A cell containing anonymous functions mapping 
%                u_n -> u_{n+1} with input (u_n,h,dW_n).
% normFuns     - A cell containing anonymous functions mapping
%                (u_ref,u_coarse) to a complex scalar.
% normOfRef    - A boolean value. If true, then the norm functions are also
%                called using (u_ref,u_ref). If only one N value is given,
%                then all norms are evaluated using (u_ref^1,u_ref^j), for
%                j = 1:length(normFuns).
% sampleOffset - Used to offset the initialization of the rng used for the
%                Brownian motion. Called with rng(m+sampleOffset,'twister')
% scriptName   - Name of the file containing the specifics of the
%                experiment. The file will be read as a text file, split
%                it along '!!', and choose the second part of the file to
%                compare with the samples already present. If no '!!'
%                delineation exists, then it will save an empty string.

% Process the parameters
numN = length(NVec); numSchemes = length(schemes); numNorms = length(normFuns);
hVec = T./NVec; refN = NVec(end); refh = hVec(end);

%% Get the specs if present (used in backups) - Surround specs with '!!'
% Load the calling file as text and extract the experiment specification
scriptString = fileread(scriptName);
splitScript = strsplit(scriptString,'!!');
% The specs are assumed to be the second partition, and if there is no
% second partition then the check will be excluded
if length(splitScript) > 1
    specs = splitScript{2};
else
    specs = '';
end

%% Check for previous samples and compare specs
specFileName = [backupName 'Specs.mat'];
% Check if samples present
if isfile(specFileName)
    % Load the specs
    temp = load(specFileName);
    
    % Inform if spec, sample offset is different, or if batchsize is
    % smaller than the present samples
    if isequal(temp.variable.specs,specs)
        fprintf('\nSpecs for present samples OK.\n')
    else
        error('Specs different for existing samples.')
    end
    
    if temp.variable.indexOffset ~= sampleOffset
        error('Sample offset not same')
    end
    
    if temp.variable.batchSize >= batchSize
        fprintf(['Not asking for more samples than already present.\n' ...
            num2str(batchSize) ' samples with offset ' num2str(sampleOffset) '.\n'])
        return
    end
end

%% Initialize storage
normStorage = cell(batchSize,length(NVec)-1);

% If we are asking for two outputs: Give norms of finest level as well
if normOfRef
    % If we have only one N, then we're obviously interested in all schemes
    if numN == 1
        runAllSchemesAsRef = true;
    else
        runAllSchemesAsRef = false;
    end
    refNormStorage = zeros(batchSize,refN+1,1,numNorms);
else
    runAllSchemesAsRef = false;
end

% Load the already present samples into the norm storage(-s)
backupFileName = [backupName '.mat'];
if isfile(backupFileName)
    % Load the specs
    temp = load(backupFileName);
    numPresSamples = size(normStorage,1);
    normStorage(1:numPresSamples,:) = temp.variable.normStorage;
    if normOfRef
        refNormStorage(1:numPresSamples,:,:) = temp.variable.refNormStorage;
    end
    clear temp
else
    numPresSamples = 0;
end

%% Perform calculations
parfor m = (numPresSamples+1):batchSize
    % Load external variables in order to enable parfor
    normFuns; schemes; NVec; hVec;
    
    % Simulate and coarsen sample Brownian motion
	rng(m+sampleOffset,'twister')
%     refW = randn(refN,1)*sqrt(refh); retBMs = coarseBM(refW,NVec);
    refW = randn(refN,2)*sqrt(refh/2); retBMs = coarseBM(refW,NVec);
    
    % Initialize memory - Norm storage
    tempNorms = cell(numN-1,1);
    for i = 1:(numN-1)
        tempNorms{i} = zeros(NVec(i)+1,numSchemes,numNorms);
    end
    if normOfRef
        % If reference norms are asked for
        if runAllSchemesAsRef
            refNorms = zeros(refN+1,numSchemes,numNorms);
        else
            refNorms = zeros(refN+1,1,numNorms);
        end
    end
    
    % Initialize memory - Initial values
    refSol = u0;
    if runAllSchemesAsRef
        refSols = cell(numSchemes,1);
    end
    coarseSol = cell(numN-1,numSchemes);
    
    % Set remaining initial values, and calculate initial value norms
    for k = 1:numNorms
        for j = 1:numSchemes
            for n = 1:(numN-1)
                coarseSol{n,j} = refSol;
                    tempNorms{n}(1,j,k) = normFuns{k}(refSol,coarseSol{n,j});
            end
            if normOfRef
                if runAllSchemesAsRef
                    refSols{j} = refSol;
                    refNorms(1,j,k) = normFuns{k}(refSols{1},refSols{j});
                else
                    refNorms(1,1,k) = normFuns{k}(refSol,refSol);
                end
            end
        end
    end
    
    % Simulate at reference precision, but step coarse solutions when the
    % correct time has been passed.
    dWIndex = ones(numN-1,1);
    scalingFactor = refN./NVec;
    for i = 1:refN
        % Step the reference solution/-s
        if runAllSchemesAsRef
            for j = 1:numSchemes
                % First scheme used as reference scheme
                refSols{j} = schemes{j}(refSols{j},refh,refW(i,:));
                % Calculate norms
                for k = 1:numNorms
                    refNorms(i+1,j,k) = normFuns{k}(refSols{1},refSols{j});
                end
            end
        else
            % First scheme used as reference scheme
            refSol = schemes{1}(refSol,refh,refW(i,:));
%             % Plot reference solution
%             figure(1)
%             clf
%             plotProfile(parStr.x,ifft(refSol),false,false)
%             pause(0.01)
            if normOfRef
                % Calculate norms
                for k = 1:numNorms
                    refNorms(i+1,1,k) = normFuns{k}(refSol,refSol);
                end
            end
        end
        
        % Exclude the finest N (which is the reference)
        for n = 1:(numN-1)
            % Step coarse solution if enough steps passed
            if mod(i,scalingFactor(n)) == 0
                for j = 1:numSchemes
                    coarseSol{n,j} = schemes{j}(coarseSol{n,j},hVec(n),...
                        retBMs{n}(dWIndex(n),:));
                    % Calculate norms
                    for k = 1:numNorms
                        tempNorms{n}(dWIndex(n)+1,j,k) = normFuns{k}(refSol,coarseSol{n,j});
                    end
                end
                dWIndex(n) = dWIndex(n) + 1;
            end
        end
        fprintf(['-------'...
            '\n Sample: ', num2str(m+sampleOffset), ...
            ' between ', num2str(sampleOffset), ' and ', num2str(sampleOffset+batchSize) ...
            '.\n Percentage until max time: ', num2str(i/refN), ...
            '.\n-------\n'])
        
%             backupSave(!!! PLACEHOLDER,normStorage)
    end
    normStorage(m,:) = tempNorms;
    if normOfRef
        refNormStorage(m,:,:,:) = refNorms;
    end
end

% Save the results to files (specs and sample ID's separately)
% Specs & sample
backUpSpecs.specs = specs;
backUpSpecs.batchSize = batchSize;
backUpSpecs.indexOffset = sampleOffset;
% Norms
backUpData.normStorage = normStorage;
if normOfRef
    backUpData.refNormStorage = refNormStorage;
end
% Save
backupSave([backupName 'Specs'],backUpSpecs)
backupSave(backupName,backUpData)
end