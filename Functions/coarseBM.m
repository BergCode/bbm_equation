function retBMs = coarseBM(dW,NVec)
% Using a fine Brownian motion with the same length as the last element in
% NVec, calculate all coarser variants

    numN = length(NVec);
    refN = NVec(end);
    retBMs = cell(numN,1);
    % If we haven't taken a splitting into account
    
    if size(dW,2) == 1
        for n = 1:numN
            currN = NVec(n);
            scalingFactor = refN/currN;
            retBMs{n} = zeros(currN,1);
            for i = 1:currN
                indexList = ((i-1)*scalingFactor+1):(i*scalingFactor);
                retBMs{n}(i) = sum(dW(indexList));
            end
        end
    else
        for n = 1:numN
            currN = NVec(n);
            scalingFactor = refN/currN;
            retBMs{n} = zeros(currN,2);
            currIndex = 0;
            for i = 1:currN
                indexList = (currIndex+1):(currIndex+scalingFactor/2);
                retBMs{n}(i,1) = sum(sum(dW(indexList,:)));
                indexList = (currIndex+scalingFactor/2+1):(currIndex+scalingFactor);
                retBMs{n}(i,2) = sum(sum(dW(indexList,:)));
                currIndex = currIndex + scalingFactor;
            end
        end
    end
end