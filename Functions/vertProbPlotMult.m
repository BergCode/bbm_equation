function vertProbPlotMult(storage,NVec,topTitle,rightTitle,varargin)
% vertProbPlotMult  - Vertical histograms with titles.
% Syntax: vertHistPlot(storage,schemeShortNames,yAxisVector,leftTitle,axesInfo)
%
% Input:
% storage           - 
% schemeShortNames  - A cell vector of length m containing strings.
% yAxisVector       - A vector containing the y-axis labels.
% leftTitle         - A string containing the left side title.
% varargin          - If storage has dim 4, accept line styles

    [fontSize,markerSize,~] = AMSFontSize;

    numRight = size(storage,1);
    numTop = size(storage,2);
    numN = size(storage,3);
    numSchemes = size(storage,4);
    
    % ERROR if wrong dimension
    if numTop ~= length(topTitle) || numRight ~= length(rightTitle)
        error(['The numer of titles does not match the number of data.'...
            '\n Number of column values: ' num2str(numTop)...
            '\n Number of top titles: ' num2str(length(topTitle))...
            '\n Number of row values: ' num2str(numRight)...
            '\n Number of right titles: %s'], num2str(length(rightTitle)))
    end
    if numN ~= length(NVec)
        error('Length of NVec and storage dimension 3 not equal.')
    end
    if numSchemes ~= length(varargin{1})
        error('Length of line styles and storage dimension 4 not equal.')
    end
    
    figure
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    
    
    edgeMargins = [0.1 0.1 0.15 0.1]; % left, right, bottom, top
    gapMargins = [0.02 0.02];
    axh = (1-sum(edgeMargins(3:4))-(numRight-1)*gapMargins(1))/numRight; 
    axw = (1-sum(edgeMargins(1:2))-(numTop-1)*gapMargins(2))/numTop;
    for j = 1:numRight
        for n = 1:numTop
            px = edgeMargins(1) + (n-1)*axw + gapMargins(1)*(n-1);
            py = edgeMargins(3) + (numRight-j)*axh + gapMargins(2)*(numRight-j);
            axes('Units','normalized', ...
            'Position',[px py axw axh], ...
            'XTickLabel','', ...
            'YTickLabel','')
%             subplot(numRight,numTop,(j-1)*numTop+n);
            % Plot probabilities
            if nargin == 4
                plot(NVec,squeeze(storage(j,n,:)),'LineWidth',2.5);
            elseif nargin == 6
                hold on
                for i = 1:numSchemes
                    plot(NVec,squeeze(storage(j,n,:,i)),varargin{1}{i},'color',varargin{2}(i,:),'LineWidth',2.5,'MarkerSize',markerSize);
                end
                hold off
            else
                error('Wrong number of input arguments.')
            end
            ylim([-.01 1.1])
            xlim([min(NVec)/1.1 max(NVec)*1.1])
            
            set(gca, 'XScale', 'log')
            set(gca,'FontSize',fontSize)
            
            % If leftmost row add probability label, otherwise remove ticks
            if(n == 1)
                ylabel('$P$','Interpreter','latex','Rotation',0)
                    yticks([0 0.5 1])
                yticklabels({'0' '0.5' '1'})
%                 ytickangle(90)
            else
                set(gca,'YTickLabel',[])
            end
            % If bottom row add N label, otherwise remove ticks
            if(j == numRight)
                xlabel('$N$','Interpreter','latex')
                labelIndexes = [2 numN-1];
                xticks([NVec(labelIndexes(1)) NVec(labelIndexes(2))])
                lab1 = ['2^{' num2str(log2(NVec(labelIndexes(1)))) '}'];
                lab2 = ['2^{' num2str(log2(NVec(labelIndexes(2)))) '}'];
                xticklabels({lab1,lab2})
            else
                set(gca,'XTickLabel',[])
            end
            
            
            % If rightmost row add right titles adapting to the subplot positions
            if(n == numTop)
                posInfo = get(gca,'position');
                posVec = [posInfo(1)+1.02*posInfo(3) posInfo(2)+0.65*posInfo(4) 0.05 0];
                annotation('textbox',posVec,...
                    'String',rightTitle{j},...
                    'EdgeColor','none',...
                    'Interpreter','latex',...
                    'FontSize',fontSize)
            end
            
            % If topmost row add top titles adapting to the subplot positions
            % Set top titles by adapting to the subplot positions
            if(j == 1)
                posInfo = get(gca,'position');
                posVec = [posInfo(1)+0.3*posInfo(3) posInfo(2)+1.3*posInfo(4) 0 0];
                annotation('textbox',posVec,...
                    'String',topTitle{n},...
                    'EdgeColor','none',...
                    'Interpreter','latex',...
                    'FontSize',fontSize)
            end
        end
    end
end