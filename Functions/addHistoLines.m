function addHistoLines(tf)
% Add lines to the vertical histogram plots produced by vertHistPlotMult.m
numCh = length(tf.Children);

% Loop over each child of the histogram plot mark out the zero line
for i = 1:numCh
    isHisto = isa(tf.Children(i).Children,...
        'matlab.graphics.chart.primitive.Histogram');
    if isHisto
        axes(tf.Children(i)) % Annoyingly puts the chosen axis first
        pause(0.01)
        
        % Identift which bin contains the zero
        [~,ZeroIndex] = find(tf.Children(1).Children.BinEdges < 0,1,'last');
        
        % Plot a line that does not cover the bin
        hold on
        plot([0,0],[tf.Children(1).Children.Values(ZeroIndex),...
            tf.Children(1).YLim(2)],'--k')
        hold off
%         pause(0.1)
    end
end
end