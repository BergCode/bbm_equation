function [colMat,schemeMarkers] = SchemePlotInfo(schemeNames)
% This function is designed to assign specific colors and markers to the 8
% schemes labeled 'SE','LT','St','EE','BE','ME','MP', and 'CN'. Any other
% scheme names provided will be assigned the color black and marker '.'.
% Input: A cell containing the scheme names, of any order.
% Output: A matrix containing the corresponding colors and a cell
% containing the corresponding markers.

numSchemes = length(schemeNames);
% See https://se.mathworks.com/matlabcentral/fileexchange/29702-generate-maximally-perceptually-distinct-colors
% for choices of distinct colors
tempColMat = ...
[         0         0    1.0000;
    1.0000         0         0;
         0    1.0000         0;
         0         0    0.1724;
    1.0000    0.1034    0.7241;
    1.0000    0.8276         0;
         0    0.3448         0;
    0.5172    0.5172    1.0000]; % Colors chosen for these 8 schemes


% Initialize return values
colMat = zeros(numSchemes,3);
schemeMarkers = cell(numSchemes,1);
% Fill in the desired colors and markers
for sch = 1:numSchemes
    switch schemeNames{sch}
        case 'SE'
            colMat(sch,:) = tempColMat(1,:);
            schemeMarkers{sch} = 'd';
        case 'LT'
            colMat(sch,:) = tempColMat(2,:);
            schemeMarkers{sch} = 'v';
        case 'St'
            colMat(sch,:) = tempColMat(3,:);
            schemeMarkers{sch} = '^';
        case 'EE'
            colMat(sch,:) = tempColMat(4,:);
            schemeMarkers{sch} = '>';
        case 'BE'
            colMat(sch,:) = tempColMat(5,:);
            schemeMarkers{sch} = '<';
        case 'ME'
            colMat(sch,:) = tempColMat(6,:);
            schemeMarkers{sch} = 's';
        case 'MP'
            colMat(sch,:) = tempColMat(7,:);
            schemeMarkers{sch} = '*';
        case 'CN'
            colMat(sch,:) = tempColMat(8,:);
            schemeMarkers{sch} = 'o';
        otherwise
            colMat(sch,:) = [0,0,0];
            schemeMarkers{sch} = '.';
    end
end
end