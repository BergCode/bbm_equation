function nextU = DetExplInt(currU,A,AB,h,p)
    nextU = exp(AB*h).*(currU+...
        A*h/(p+1).*fft(ifft(currU).^(p+1)));
end