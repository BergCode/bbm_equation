function nextU = DetStrangSpl(currU,A,AB,h,p)
    % Linear step (explicit)
    midU = exp(AB*h/2).*currU;
    % Nonlinear step (implicit)
    tempU = midU;
    realSpaceNextU = ifft(midU);
    realSpaceMidU = realSpaceNextU;
    i = 1;
    crit = true;
    K = length(currU);
    while crit && i < 120
        av_val = (realSpaceNextU+realSpaceMidU)/2;
        nextU = midU + h*A.*fft(av_val.^(p+1)/(p+1));
        crit = norm((tempU-nextU)./K,2) > eps;
        tempU = nextU;
        realSpaceNextU = ifft(nextU);
        i = i+1;
    end
    % Linear step (explicit)
    nextU = exp(AB*h/2).*nextU;
end