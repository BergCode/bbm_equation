function nextU = DetThetaExp(currU,A,AB,h,p,theta)
    % Theta = 1: EExp
    % Theta = 0: BExp
    % Theta = 1/2: MExp
    
    % Precalc
    term1 = exp(AB*h).*currU;
    term2 = exp(AB*h).*A*h/(p+1);
    
    nextU = currU;
    % Solve the implicit equation
    crit = true;
    M = length(currU);
    i = 1;
    while crit && i<120
        oldU = nextU;
        nextU = term1 + term2.*fft(...
                (theta*ifft(currU) + (1-theta)*ifft(nextU)).^(p+1));
        crit = norm((oldU-nextU)./M,2) > eps;
    end
end