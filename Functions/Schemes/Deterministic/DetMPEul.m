function nextU = DetMPEul(currU,k,kSq,h,p,s,q,r)
    nextU = DetBBMImplSolver(currU,k,kSq,h,p,s,q,r,@(un,unext) ((un+unext)/2).^(p+1));
end