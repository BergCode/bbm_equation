function nextU = DetCNEul(currU,k,kSq,h,p,s,q,r)
    nextU = DetBBMImplSolver(currU,k,kSq,h,p,s,q,r,@(un,unext) (un.^(p+1)+unext.^(p+1))/2);
end