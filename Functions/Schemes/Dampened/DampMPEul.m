function nextU = DampMPEul(currU,k,kSq,dW,h,p,s,r)
    nextU = DampBBMImplSolver(currU,k,kSq,sum(dW),h,p,s,r,@(un,unext) ((un+unext)/2).^(p+1));
end