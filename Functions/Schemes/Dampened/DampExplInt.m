function nextU = DampExplInt(currU,A,B,dW,h,p)
    nextU = exp(A*sum(dW) + B*h).*(currU+...
        A*h/(p+1).*fft(ifft(currU).^(p+1)));
end