function nextU = DampSExp(currU,A,B,dW,h,p)
    a = exp(A*dW(1) + B*h/2).*currU; % Implicit pre-solve
    % Solve the implicit equation
    crit = true;
    M = length(currU);
    NStar = currU;
    i = 1;
    while crit && i<120
        oldNStar = NStar;
        tempNStar = a+h/2*NStar;
        NStar = A/(p+1).*fft((ifft(tempNStar).^(p+1)));
        crit = norm((oldNStar-NStar)./M,2) > eps;
        i = i+1;
    end
    % Perform the explicit part
    nextU = exp(A*sum(dW) + B*h).*currU + h*exp(A*dW(2) + B*h/2).*NStar;
end