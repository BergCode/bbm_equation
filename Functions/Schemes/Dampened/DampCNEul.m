function nextU = DampCNEul(currU,k,kSq,dW,h,p,s,r)
    nextU = DampBBMImplSolver(currU,k,kSq,sum(dW),h,p,s,r,@(un,unext) (un.^(p+1)+unext.^(p+1))/2);
end