function nextU = LTSpl(currU,A,dW,h,p)
    % Linear step (explicit)
    midU = exp(A*sum(dW)).*currU;
    % Nonlinear step (implicit)
    tempU = midU;
    realSpaceNextU = ifft(midU);
    i = 1;
    crit = true;
    K = length(currU);
    while crit && i < 120
        nextU = midU + h*A.*fft(realSpaceNextU.^(p+1)/(p+1));
        crit = norm((tempU-nextU)./K,2) > eps;
        tempU = nextU;
        realSpaceNextU = ifft(nextU);
        i = i+1;
    end
end