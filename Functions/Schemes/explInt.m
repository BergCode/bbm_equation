function nextU = explInt(currU,A,dW,h,p)
    nextU = exp(A*sum(dW)).*(currU+...
        A*h/(p+1).*fft(ifft(currU).^(p+1)));
end