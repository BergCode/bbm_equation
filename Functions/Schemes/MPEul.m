function nextU = MPEul(currU,k,kSq,dW,h,p)
    nextU = BBMImplSolver(currU,k,kSq,sum(dW),h,p,@(un,unext) ((un+unext)/2).^(p+1));
end