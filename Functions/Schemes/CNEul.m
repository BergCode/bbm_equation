function nextU = CNEul(currU,k,kSq,dW,h,p)
    nextU = BBMImplSolver(currU,k,kSq,sum(dW),h,p,@(un,unext) (un.^(p+1)+unext.^(p+1))/2);
end