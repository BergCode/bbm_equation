function ret = H1norm(u,dx,k,per,varargin)
% H1norm - An approximation of the H1 norm.
% Syntax: ret = H1norm(u,dx,k,per,)
%
% Input:
% u         - A vector of length M containing the function u in Fourier space.
% dx        - The space step size.
% k         - A vector of length M containing the Fouier modes.
% per       - A boolean value declaring whether u periodic or not.
% varargin  - A pair of values:
%             1. A boolean flag for whether the Simpsons integral 
%             approximation should be used, or whether the Parseval theorem
%             info should be used (false for Parseval) (~5 times faster for 
%             M=2000, and increasing).
%             2. Interval length
%
% Output:
% ret - int abs(u)^2 + abs(d/dx u)^2 dx
%
% Non-standard dependencies: L2norm.m, absSq.m.
% See also: L2norm.m

    % Use Simpsons integral by default
    if nargin > 4 && ~varargin{1}
        M = length(u);
        L = varargin{2};
        ret = sum((1+k.^2).*absSq(u))/M^2*L;
    else
        ret = L2norm(ifft(u),dx,per,true) + L2norm(ifft(1i*k.*u),dx,per,true);
    end
end