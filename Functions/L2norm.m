function ret = L2norm(u,dx,per,varargin)
% L2norm - An approximation of the L2 norm, or the squared absolute integral.
% Syntax: ret = L2norm(u,dx,per,varargin)
%
% Input:
% u         - A vector containing the function u in physical space
% dx        - The space step size.
% per       - A boolean value declaring whether u periodic or not
% varargin  - A pair of values, only applicable if periodic
%             1. A boolean flag for whether the Simpsons integral 
%             approximation should be used, or whether the Parseval theorem
%             info should be used (false for Parseval) (~2 times faster for 
%             M=2000, and increasing, less precise for small M)
%             Assumes that u is in Fourier space and periodic if false.
%             2. Interval length
%
% Output:
% ret - int abs(u)^2 dx
%
% Non-standard dependencies: absSq.m, simpsonIntegral.m
% See also: PSHist.m for example usage.
%           H1norm.m

    % Check that u is a vector
    if size(u,1) ~= 1 && size(u,2) ~= 1
        error('u not vector, required for L2 norm approximation')
    end
    
    if per
        % Simpsons integral by default
        if nargin > 3 && ~varargin{1}
            M = length(u);
            L = varargin{2};
            ret = sum(absSq(u))/M^2*L;
        else
            % Append first value
            if size(u,1) == 1
                ret = simpsonIntegral(dx,absSq([u u(1)]));
            else
                ret = simpsonIntegral(dx,absSq([u;u(1)]));
            end
        end
    else
        ret = simpsonIntegral(dx,absSq(u));
    end
end