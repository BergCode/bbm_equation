function vertHistPlotMult(storage,schemeShortNames,yAxisVector,topTitle,rightTitle,varargin)
% vertHistPlot  - Vertical histograms with titles.
% Syntax: vertHistPlot(storage,schemeShortNames,yAxisVector,leftTitle,axesInfo)
%
% Input:
% storage           - 
% schemeShortNames  - A cell vector of length m containing strings.
% yAxisVector       - A vector containing the y-axis labels.
% leftTitle         - A string containing the left side title.
% varargin          - A vector containing the yticks
%                     A cell containing the ylabels
%                     A boolean value for norming histogram height
%
% Non-standard dependencies: None.
% See also: PSHist.m for example usage.
    [fontSize,~,~] = AMSFontSize;

    % Plot a temporary figure which will help with the histogram axes
    tempFig = figure('visible','off');
    histogram(1,yAxisVector)
    axXLim = tempFig.CurrentAxes.XLim;
    pause(0.5)
    clear tempFig
    
    numSchemes = size(storage,4);
    numTop = size(storage,2);
    numRight = size(storage,3);
    
    % ERROR if wrong number of titles
    if numTop ~= length(topTitle) || numRight ~= length(rightTitle)
        error(['The numer of titles does not match the number of data.'...
            '\n Number of column values: ' num2str(numTop)...
            '\n Number of top titles: ' num2str(length(topTitle))...
            '\n Number of row values: ' num2str(numRight)...
            '\n Number of right titles: %s'], num2str(length(rightTitle)))
    end
    
    figure
    [figWidth,figHeight] = AMSFormatScreenSize;
    figPos = [0,0,figWidth,figHeight];
    set(gcf,'Units','Inches','Position',figPos);
    tiledlayout(numRight,numTop,'TileSpacing','compact')
    
    % Initialize all tiles and store their positions
    tilePos = cell(numTop,numRight);
    tileID = cell(numTop,numRight);
    for j = 1:numRight
        for n = 1:numTop
            tempTile = nexttile((j-1)*numTop + n);
            set(gca,'XTickLabel',[])
            set(gca,'YTickLabel',[])
            tilePos{n,j} = tempTile.Position;
            tileID{n,j} = tempTile;
        end
    end

    % Create null titles for erasing some labels
    nullTitles = cell(numSchemes,1);
    for i = 1:numSchemes
        nullTitles{i} = '';
    end
    
    % Set top titles by adapting to the subplot positions
    for n = 1:numTop
%         posInfo = get(subplot(numRight,numTop,n),'position');
%         tempTile = nexttile;
        posInfo = tilePos{n,1};
        annotation('textbox',...
        min([1,1,1,1],[posInfo(1)+0.2*posInfo(3) posInfo(2)+1.3*posInfo(4) 0 0]),...
            'String',topTitle{n},...
            'EdgeColor','none',...
            'Interpreter','latex',...
            'FontSize',fontSize)
    end
    
    % Set right titles by adapting to the subplot positions
    for j = 1:numRight
%         posInfo = get(subplot(numRight,numTop,j*numTop),'position');
%         tempTile = nexttile(j*numTop);
        posInfo = tilePos{numTop,j};
        annotation('textbox',[posInfo(1)+1.02*posInfo(3) posInfo(2)+0.65*posInfo(4) 0.1 0],...
            'String',rightTitle{j},...
            'EdgeColor','none',...
            'Interpreter','latex',...
            'FontSize',fontSize)
    end
    
    % Carry the axis information to after histogram plotting
    axesInfo = cell(numRight,numTop,numSchemes);
    histogramMaxAxis = 0;
    for j = 1:numRight
        for n = 1:numTop
%             currFig = subplot(numRight,numTop,(j-1)*numTop+n);
%             currFig = nexttile((j-1)*numTop+n);
            axes(tileID{n,j});
            % If it's the leftmost row, we add time labels, else remove
            if(n == 1)
                ylim([yAxisVector(1) yAxisVector(end)])
                if nargin > 5
                    yticks(varargin{1})
%                     yticklabels(varargin{2}) % Old version, not Latex
                    set(gca, 'YTickLabel', varargin{2}, 'TickLabelInterpreter', 'latex');
                else
                    yticks([yAxisVector(1) (yAxisVector(1)+yAxisVector(end))/2 yAxisVector(end)])
                    yticklabels({'0', 'T/2', 'T'})
                end
                pause(0.01)
            else
                set(gca,'YTickLabel',[], 'TickLabelInterpreter', 'latex')
            end
            % If it's the bottom row, we add scheme names, else remove
            if(j == numRight)
                axis([ 0 numSchemes axXLim])
                xticks((0:(numSchemes-1))+0.5)
                xticklabels(schemeShortNames)
            else
                set(gca,'XTickLabel',[])
            end
            set(gca,'FontSize',fontSize)
            % Plot histograms
%             posInfo = get(currFig,'position');
            posInfo = tilePos{n,j};
            posInfo(3) = posInfo(3)/numSchemes;
            for i = 1:numSchemes
                axes('Position',[posInfo(1)+(i-1)*posInfo(3) posInfo(2) posInfo(3) posInfo(4)])
                histogram(storage(:,n,j,i),yAxisVector);
                % Get current axis for later modification if necessary
                axesInfo{j,n,i} = gca;
                % Get maximum observed axis value, if present
                histogramMaxAxis = max(histogramMaxAxis,axesInfo{j,n,i}.YLim(2));
%                 ylim([0 size(storage,1)])
                set(gca,'view',[90 -90])
                set(gca,'XTick',[])
                set(gca,'YTick',[])
                set(gca,'YTickLabel',[])
                set(gca,'XTickLabel',[])
                pause(0.01)
            end
        end
    end
    
    % Ensure uniform histogram axes, if any data visible
    if nargin > 7 && varargin{3}
        for j = 1:numRight
            for n = 1:numTop
                for i = 1:numSchemes
                    if axesInfo{j,n,i}.YLim(1) > -1
                        axesInfo{j,n,i}.YLim(2) = histogramMaxAxis;
                    end
                end
            end
        end
    end
end