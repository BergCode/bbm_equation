function probConvMult(schemeErrors,cPotVec,...
    deltaVec,NVec,hVec,schemeMarkers,schemeColors)
%MULTPROBCONV Summary of this function goes here
%   Detailed explanation goes here
numC = length(cPotVec);
numDelta = length(deltaVec);
numN = length(NVec);
numSchemes = length(schemeMarkers);

% Storage
probConvStorage = zeros(numC,numDelta,numN,numSchemes);

for i = 1:numC
    for j = 1:numDelta
        for n = 1:numN
            for scheme = 1:numSchemes
                % Check how large the probability is that the errors
                % exceed the line value
                probConvStorage(i,j,n,scheme) = ...
                    mean(...
                        schemeErrors(:,n,scheme) > (10^cPotVec(i))*(hVec(n).^deltaVec(j))...
                    );
            end
        end
    end
end

% Initialize title information
rightTitleFlow = cell(numC,1);
topTitleFlow = cell(numDelta,1);

for j = 1:numC
    rightTitleFlow{j} = ['$C = $ $10^{' num2str(cPotVec(j)) '}$'];
end
for j = 1:numDelta
    topTitleFlow{j} = ['$\delta = ' num2str(deltaVec(j)) '$'];
end

vertProbPlotMult(probConvStorage,NVec,topTitleFlow,rightTitleFlow,...
    schemeMarkers,schemeColors)
end