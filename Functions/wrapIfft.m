function res = wrapIfft(y)
% Calculates the ifft and appends the boundary at the start to the end
temp = ifft(y);
res = [temp temp(1)];
end

