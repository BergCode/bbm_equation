% Collision of 2 solitons with opposite amplitudes
currFileName = 'soliton_evolution_2.m';
% [fList,pList] = matlab.codetools.requiredFilesAndProducts(currFileName);

%% !! Experiment Specs - Identifies unique experiment
% Time info
T = 40; % Time horizon
N = 2^16; % Number of time steps
h = T/N;

% Spatial info
M = 2^13; % Number of spatial points / Fourier modes
L = 150; % Interval radius
x_int = [-L,L]; % Interval
int_width = x_int(2) - x_int(1);
dx = (x_int(2)-x_int(1))/M; % Spatial grid step size
x = x_int(1) + dx*(0:M-1); % Spatial grid
k = 2*pi/(x_int(2)-x_int(1))*[0:M/2, -M/2+1:-1]; % Fourier modes

% Pre-calc, used in schemes
k_sq = absSq(k);
A = -1i*k./(1+k_sq);

% Soliton info - Initial value
p = 1; % Nonlinearity power
% p_vec = [1,2,3]; % Nonlinearity powers
c_vec = [3,-2];
x_0_vec = [-50,50];
IV = @(x,c,x_0) 3*(c-1)*sech(.5*sqrt(1-1/c)*(x-x_0)).^2; % Initial value - Soliton
% backupName = 'Data/ErrorEstimates/normEstimates2022Apr'; % Data file name


% Scheme info
schemes = { @(currU) DetSExp(currU,A,A,h,p),...
            @(currU) DetExplInt(currU,A,A,h,p),...
            @(currU) DetMPEul(currU,k,k_sq,h,p,0,1,0)};
num_schemes = length(schemes);
scheme_names = {'SE','EE','MP'};
[col_mat,scheme_markers] = SchemePlotInfo(scheme_names);

% !! End of experiment specs
%% Query storage - Evolution, as well as H1, energy, and mass
% Query fidelity
time_fidelity = 2^10;
space_fidelity = 2^12;

if N > time_fidelity
    time_scale_factor = N / time_fidelity;
    t_indexes = 0:time_scale_factor:N;
else
    time_scale_factor = 1;
    t_indexes = 0:N;
end
if M > space_fidelity
    space_scale_factor = M / space_fidelity;
    space_indexes = 1:space_scale_factor:M;
else
    space_indexes = 1:M;
end

% Initialize the evolution storage
u_storage = zeros(length(t_indexes),length(space_indexes),num_schemes);
query_index = 2;

% Define the functions that we want to see the drift of
energy = @(x) trapz(x.^3+3*x.^2)*dx;
H1_fun = @(x) H1norm(x,dx,k,true,false,int_width);
mass_fun = @(x) trapz(wrapIfft(x))*dx;
energy_fun = @(x) energy(wrapIfft(x));

drift_funs = {H1_fun,mass_fun,energy_fun};
num_drifts = length(drift_funs);
drift_names = {'$||u||^2_{H^1}$','$M(u)$','$H(u)$'};

% Initialize the drift storage
drift_storage = zeros(N+1,num_drifts,num_schemes);

%% Simulate experiment
% Create initial values and calculate original function values
u_0 = zeros(1,M);
max_height = 0;
for i = 1:length(c_vec)
    u_0 = u_0 + IV(x,c_vec(i),x_0_vec(i));
    max_height = max(max_height,3*(c_vec(i)-1));
end
orig_u_0 = u_0;
u_0 = fft(u_0);

curr_u = cell(num_schemes,1);
for sch = 1:num_schemes
    curr_u{sch} = u_0;
    u_storage(1,:,sch) = orig_u_0(space_indexes);
end

for drift = 1:num_drifts
    drift_storage(1,drift,:) = drift_funs{drift}(u_0);
end

% Plot only a fraction of the steps
plot_run = false;
num_plots = 2^7;
plot_frac = N/num_plots;

if plot_run
    figure(1)
    clf
    tiledlayout(1,3,"TileSpacing","compact","Padding","compact")
end
for i = 1:N
    for sch = 1:num_schemes
        curr_u{sch} = schemes{sch}(curr_u{sch});
        
        % Calculate the drift functions
        for drift = 1:num_drifts
            drift_storage(i+1,drift,sch) = drift_funs{drift}(curr_u{sch});
        end

        % Store the solution if at the correct time step
        if mod(i,time_scale_factor) == 0
            temp_curr_u = ifft(curr_u{sch});
            u_storage(query_index+1,:,sch) = temp_curr_u(space_indexes);
            % Only progress the query index at the last scheme
            if sch == num_schemes
                query_index = query_index+1;
                disp(['Fraction of time ' num2str(i/N)])
            end
        end
        % Plot if we want to observe the trajectories
        if plot_run && mod(i,plot_frac) == 1
            nexttile(sch)
            plot(x,real(ifft(curr_u{sch})))
            ylim([-2,max_height])
            pause(0.01)
        end
    end
end
disp('Done')

%% Plot endpoint
figure(num_schemes+1)
clf
tiledlayout(1,3,"TileSpacing","compact","Padding","compact")
min_point = 0;
for sch = 1:num_schemes
%     min(ifft(curr_u{sch}))
    min_point = min(min_point,min(real(ifft(curr_u{sch}))));
end
for sch = 1:num_schemes
    nexttile(sch)
    plot(x,real(ifft(curr_u{sch})))
%     plot(x,abs(real(ifft(curr_u{sch}))))
%     set(gca,'YScale','log')
%     ylim([min_point,0.03])
end

%% Plot evolution & drifts
[font_size,marker_size,line_width] = AMSFontSize;
plot_name = 'Plots/Deterministic/Soliton/Evol_and_drift_PS_';
t_vec = linspace(0,T,time_fidelity+2);
fine_t_vec = linspace(0,T,N+1);
x_vec = x(space_indexes);

% Exclude mass, since it's not interesting for now
fun_indexes = [1,3];

for sch = 1:num_schemes
    temp_evol = u_storage(:,:,sch);
    scheme_max_val = max(real(temp_evol(:)));
    scheme_min_val = min(real(temp_evol(:)));
    % Export the plotting to make it more readable
    figure(sch)
    clf
    tiledlayout(1,2,"TileSpacing","tight","Padding","compact")

    % Top-down evolution
    nexttile()
    z_lim = [scheme_min_val,scheme_max_val];
    fig_uniform(x_vec,t_vec,real(u_storage(:,:,sch)),z_lim)
    xlim([-100,100])

    % Drifts
    nexttile()
    plot(abs(drift_storage(:,fun_indexes,sch)-drift_storage(1,fun_indexes,1)),...
        fine_t_vec,'LineWidth',line_width)
    set(gca,'XScale','log')
    % EE underperforms in terms of drift
    if sch ~=2
        xlim([10^-14,10^-2])
    end
    set(gca,'YTick',[])
%     ylabel('$t$','Interpreter','latex')
    xlabel('Drift','Interpreter','latex')
    legend(drift_names(fun_indexes),'Interpreter','latex','Location','southeast')
    set(gca,'FontSize',font_size)

    % Export image with transparent background
    % Get a snapshot of current look
    pause(1)
    F = getframe(gcf);
    % Export to a separate figure
    figure(num_schemes+1)
    imshow(F.cdata);
    
    I = getimage(gca);
    imwrite(I,[plot_name scheme_names{sch} '.png'])
    pause(1)
end

%% Support functions
function fig_uniform(x,y,z,z_lim)
    [font_size,marker_size,line_width] = AMSFontSize;

    % Reduce padding
%     tiledlayout(1,1,'Padding','compact')
%     nexttile

    % Produce the surfaces
    surf(x,y,z,'LineStyle','none')
    colorbar(gca,'west')

    % Adapt z axis
    zlim(z_lim)
    caxis(z_lim)

    % Set standard sizes
    set(gca,'LineWidth',line_width)
    set(gca,'FontSize',font_size)

    % Set background color
    set(gcf, 'color', 'w');

    % Set labels
    ylabel('$t$','Interpreter','latex');
    xlabel('$x$','Interpreter','latex');

    % Set camera position to top-down perspective
    view(2)

    % Enforce maximum figure size for highest possible resolution
    set(gcf,'position',[0 0 1 1],'units','normalized')
    pause(1)
end