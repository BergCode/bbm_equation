fileName = 'compTime.m';
batchSize = 200;
%% !! Experiment specs
T = 1; % Time horizon
NVec = 2.^[7:15,18]; % Number of time steps
M = 2^12; % Number of spatial points / Fourier modes

IV = @(x) 1.3*exp(-2*x.^2); % Initial value
L = 20*pi; % Interval radius
XInt = [-L,L]; % Inderval
dx = (XInt(2)-XInt(1))/M; % Spatial grid step size
x = XInt(1) + dx*(0:M-1); % Spatial grid
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2, -M/2+1:-1]; % Fourier modes
kSq = absSq(k); % Pre-calc, used in schemes
A = -1i*k./(1+kSq); % Pre-calc, used in schemes
p = 1; % Nonlinearity power
backupName = 'Data/compTimes2022Apr'; % Data file name

schemes = {...
    @(currU,h,dW) MPEul(currU,k,kSq,dW,h,p),...
    @(currU,h,dW) CNEul(currU,k,kSq,dW,h,p),...
    @(currU,h,dW) LTSpl(currU,A,dW,h,p),...
    @(currU,h,dW) StrangSpl(currU,A,dW,h,p),...
    @(currU,h,dW) explInt(currU,A,dW,h,p),...
    @(currU,h,dW) thetaExp(currU,A,dW,h,p,0),...
    @(currU,h,dW) thetaExp(currU,A,dW,h,p,0.5),...
    @(currU,h,dW) SExp(currU,A,dW,h,p)};
numSchemes = length(schemes);
schemeNames = {'MP','CN','LT','St','EE','BE','ME','SE'};
% Aiming for uniform plot colors and markers
[colMat,schemeMarkers] = SchemePlotInfo(schemeNames);

% L2, H1, LInf
normNames = {'L2','H1','LInf'};
logScaleNorms = [true,true,true,false,false,false];
normFuns = {@(x,y) sqrt(L2norm(x-y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(x-y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(x)-ifft(y)))};
numNorms = length(normFuns);
% !! End of experiment specs
%% Some light processing
numN = length(NVec)-1;
hVec = T./NVec;
refN = NVec(end);
refh = hVec(end);
%% Initialize storage & load already existing data
numPresSamples = 0;
timeStorage = zeros(batchSize,numN,numSchemes);
errorStorage = zeros(batchSize,numN,numSchemes,numNorms);

if isfile([backupName '.mat'])
    load([backupName '.mat'])

    % Make sure that the specs are the same as in the saved data
    specs = fetchSpecs(fileName);
    if ~strcmp(specs,variable.specs)
        fprintf('\nWARNING: Specs in data not consistent with specs in script.\n')
        return
    end
    % Check how many samples are already present
    numPresSamples = size(variable.timeStorage,1);

    if numPresSamples >= batchSize
        fprintf(['\n-------'...
            '\n Samples already computed to batch size ', num2str(numPresSamples), ...
            '.\n Asking for ', num2str(batchSize), '.',...
            '\n-------\n'])
        % Overwrite storage
        timeStorage = variable.timeStorage;
        errorStorage = variable.errorStorage;
        return
    end
    % Fill in the partial results
    timeStorage(1:numPresSamples,:,:) = variable.timeStorage;
    errorStorage(1:numPresSamples,:,:,:) = variable.errorStorage;
end
%% Simulation (compare at end point only)
for m = (numPresSamples+1):batchSize
    % Brownian motions
	rng(m,'twister')
    refW = randn(refN,2)*sqrt(refh/2); 
    retBMs = coarseBM(refW,NVec);
    % Initial values
    currURef = fft(IV(x));
    % Reference solution
    for i = 1:NVec(end)
        currURef = schemes{1}(currURef,refh,refW(i,:));
    end
    % Calculate the coarser solutions for each scheme
    for n = 1:numN
        for sch = 1:numSchemes
            currU = fft(IV(x));
            % Take the time of how long it will take to simulate
            tic;
            for i = 1:NVec(n)
                currU = schemes{sch}(currU,hVec(n),retBMs{n}(i,:));
            end
            timeStorage(m,n,sch) = toc;
            % Print progress
            fprintf(['-------'...
                '\n Sample: ', num2str(m), ...
                '.\n N value: ', num2str(n), ' of ', num2str(numN)...
                '.\n Scheme value: ', num2str(sch), ' of ', num2str(numSchemes)...
                '.\n-------\n'])
            % Calculate errors at end
            for norm = 1:numNorms
                errorStorage(m,n,sch,norm) = normFuns{norm}(currU,currURef);
            end
        end
    end
end

%% Rename scheme St to ST
schemeNames{4} = 'ST';

%% Save data and specs - Specs surrounded by '!!'
storages.timeStorage = timeStorage;
storages.errorStorage = errorStorage;
storages.specs = specs;
backupSave(backupName,storages);
%% Compare errors
[fontSize,markerSize,lineWidth] = AMSFontSize;
plotName = 'timeVsError';
meanTimes = squeeze(mean(timeStorage));
meanErrors = squeeze(mean(errorStorage));

figure(1)
clf
tiledlayout(1,numNorms+1,'TileSpacing','tight','Padding','tight')

% First, the computational time plot
nexttile()
% subplot(1,numNorms+1,1)
hold on
for sch = 1:numSchemes
    plot(hVec(1:end-1),meanTimes(:,sch),['-' schemeMarkers{sch}],...
        'Color',colMat(sch,:),'MarkerSize',markerSize,'LineWidth',lineWidth)
    set(gca, 'XScale', 'log', 'YScale', 'log','FontSize',fontSize)
end
legend(schemeNames{:})
xlabel('$h$',"Interpreter","latex")
% title('Comp. time (sec)',"Interpreter","latex")

% Next, the error vs time plots
axisLim = [min(meanErrors(:)),max(meanErrors(:))];
for i = 1:numNorms
    nexttile()
%     subplot(1,numNorms+1,i+1)
    hold on
    for sch = 1:numSchemes
        plot(meanTimes(:,sch),meanErrors(:,sch,i),['-' schemeMarkers{sch}],...
            'Color',colMat(sch,:),'MarkerSize',markerSize,'LineWidth',lineWidth)
    end
%     title([normNames{i} " error at $T$"],"Interpreter","latex")
    xlabel('Comp. time (sec)',"Interpreter","latex")

%     ylim(axisLim)
    set(gca, 'XScale', 'log', 'YScale', 'log','FontSize',fontSize)
%     if i ~= 1
%         set(gca,'YTickLabel',[])
%     end
end

% Save figure
pause(1)
printToPDF(gcf,plotName)