currFileName = 'errorEstimates.m';
% [fList,pList] = matlab.codetools.requiredFilesAndProducts(currFileName);

batchSize = 600;
smallerBatchSize = 50;
numSmallerBatches = batchSize/smallerBatchSize;

%% !! Experiment Specs - Identifies unique experiment
% Time info
T = 1; % Time horizon
NVec = 2.^[7:16,19]; % Number of time steps

% Spatial info
M = 2^12; % Number of spatial points / Fourier modes
L = 20*pi; % Interval radius
XInt = [-L,L]; % Interval
dx = (XInt(2)-XInt(1))/M; % Spatial grid step size
x = XInt(1) + dx*(0:M-1); % Spatial grid
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2, -M/2+1:-1]; % Fourier modes

% Pre-calc, used in schemes
kSq = absSq(k);
A = -1i*k./(1+kSq);

% Misc. info
IV = @(x) 1.3*exp(-2*x.^2); % Initial value
p = 1; % Nonlinearity power
backupName = 'Data/ErrorEstimates/normEstimates2022Apr'; % Data file name

% Scheme info
schemes = {...
    @(currU,h,dW) SExp(currU,A,dW,h,p),...
    @(currU,h,dW) LTSpl(currU,A,dW,h,p),...
    @(currU,h,dW) StrangSpl(currU,A,dW,h,p),...
    @(currU,h,dW) explInt(currU,A,dW,h,p),...
    @(currU,h,dW) thetaExp(currU,A,dW,h,p,0),...
    @(currU,h,dW) thetaExp(currU,A,dW,h,p,0.5),...
    @(currU,h,dW) MPEul(currU,k,kSq,dW,h,p),...
    @(currU,h,dW) CNEul(currU,k,kSq,dW,h,p)};
numSchemes = length(schemes);
schemeNames = {'SE','LT','St','EE','BE','ME','MP','CN'};
[colMat,schemeMarkers] = SchemePlotInfo(schemeNames);

% Norms: L2, H1, LInf
normNames = {'L2','H1','LInf','L2','H1','LInf'};
logScaleNorms = [true,true,true,false,false,false];
normFuns = {@(x,y) sqrt(L2norm(x-y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(x-y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(x)-ifft(y))),...
    @(x,y) sqrt(L2norm(y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(y)))};

% !! End of experiment specs
%% Simulate experiment
u0 = fft(IV(x));
for i = 1:numSmallerBatches
    indexOffset = (i-1)*smallerBatchSize;
    currBackupName = [backupName '(' num2str(i) ')'];
    % Samples are saved, so no need to store them for now
    ErrorCalc(NVec,T,currBackupName,u0,smallerBatchSize,schemes,...
        normFuns,false,indexOffset,currFileName);
end
%% Load available samples and extract, H1 norm drift and convergences
hVec = T./NVec;

numLogPlots = sum(logScaleNorms);
numNorms = length(normFuns);
numN = length(NVec)-1;

% Assuming that it's the first ones that are log scale plots
schemeErrors = zeros(batchSize,numN,numSchemes,numLogPlots);

% Load each smaller saple set and load into memory
for sm = 1:numSmallerBatches
    load([backupName '(' num2str(sm) ')']); % Stored in variable
    sampleFloor = (sm-1)*smallerBatchSize;
    
    for m = 1:smallerBatchSize
        for n = 1:numN
            for i = 1:numSchemes
                % Load all the maximum errors
                for norm = 1:numLogPlots
                    schemeErrors(sampleFloor+m,n,i,norm) = ...
                        max(variable.normStorage{m,n}(:,i,norm)); 
                end
            end
        end
    end
    fprintf(['\nLoaded batch ' num2str(sm) ' of ' num2str(numSmallerBatches)])
end
fprintf('\n')
clear variable

%% Rename scheme St to ST
schemeNames{3} = 'ST';

%% Mean-square convergence
plotName = 'Plots/Strong/StrongBase';

% Disable title
noTitle = cell(numLogPlots,1);
for i = 1:numLogPlots
    noTitle{i} = [];
end
% Move legend
legPos = 'southeast';

figure(1)
hPows = 1; lineChoice = 3;
meanMat = squeeze(mean(schemeErrors));
plotStrongErrors(meanMat,hVec,schemeMarkers,colMat,...
    schemeNames,noTitle,hPows,lineChoice,[],noTitle,legPos)
% Save figure
pause(1)
printToPDF(gcf,plotName,false)
%% Convergence in probability - probability plot
% Use one contant vector per norm
cPotVec = {[-2 -1.5 -1.15 -0.75]
    [-1.75 -1.5 -1.15 -0.6]
    [-1.75 -1.5 -1.15 -0.75]};
deltaVector = [.9 1 1.1];
fileNames = {'Plots/Prob/Mult/multProbL2'
    'Plots/Prob/Mult/multProbH1'
    'Plots/Prob/Mult/multProbLInf'};

for norm = 1:numLogPlots
    probConvMult(schemeErrors(:,:,:,norm),cPotVec{norm},...
        deltaVector,NVec(1:end-1),hVec,schemeMarkers,colMat)
    % Print to pdf
    pause(1)
    printToPDF(gcf,fileNames{norm},false)
end
%% Convergence in probability - estimate C(epsilon) (alt. 2)
normPlotName = 'Plots/Prob/RangeIntegral/ProbBase';
powVec = linspace(0.9,1.1,101);

% Disable title
noTitle = cell(numLogPlots,1);
for i = 1:numLogPlots
    noTitle{i} = [];
end
% Move legend
legPos = 'southeast';

% Compare with shape
% plot(powVec-1,abs(hVec(1).^(-powVec+1)-hVec(end-1).^(-powVec+1)))
% from lemma
plotProbConv(schemeErrors,hVec,powVec,colMat,noTitle,...
    schemeNames,schemeMarkers,true,legPos)

% Print to pdf
pause(0.1)
printToPDF(gcf,normPlotName,false) % Don't move to plot folder
pause(0.1)

%% Almost sure convergence
[fontSize,markerSize,lineWidth] = AMSFontSize;
normPlotName = {'Plots/AS/ASL2','Plots/AS/ASH1','Plots/AS/ASLInf'};
histPlotName = 'Plots/AS/Hist';
% Estimate the constant K_\delta(T,\omega) for each sample, and then
% compute the maximum distance to each err_{h}
numDelta = 5;
% Center around the thought convergence
diffSize = 0.5;
deltaVector = linspace(1-diffSize,1+diffSize,numDelta);
kErrorEst = zeros(batchSize,numSchemes,numDelta,numLogPlots);

for norm = 1:numLogPlots
    % logStepSize = log(dt_num(2))-log(dt_num(1));

    for m = 1:batchSize
        for i = 1:numSchemes
            for j = 1:numDelta
                % Assuming equally spaced time steps (in log space)
                % We want the line (in log space) to be normed above the
                % maximum attained error
                sampleErrors = log(schemeErrors(m,:,i,norm));
                lineEst = deltaVector(j)*log(hVec(1:end-1))+...
                    max(sampleErrors-deltaVector(j)*log(hVec(1:end-1)));
%                 kErrorEst(m,i,j,norm) = sum(absSq(sampleErrors-lineEst));
                kErrorEst(m,i,j,norm) = max(abs(sampleErrors-lineEst));

%                 figure(numLogPlots+1)
%                 plot(log(hVec(1:end-1)),sampleErrors,log(hVec(1:end-1)),lineEst,'r')
%                 title(['\delta = ' num2str(deltaVector(j))])
%                 pause(0.1)
            end
        end
    end
end

% Plot line error for AS conv. 
% Assuming that middle index is the sought convergence speed
convSpeedIndex = ceil(numDelta/2);
kErrorComp = kErrorEst - kErrorEst(:,:,convSpeedIndex,:);
meanKErr = squeeze(mean(kErrorComp));
medKErr = squeeze(median(kErrorComp));
stdKErr = squeeze(std(kErrorComp));

% Used for CI based on normal distribution
confLevel = 0.95;
perc = abs(norminv((1-confLevel)/2,0,1));

for norm = 1:numLogPlots
    h = figure(norm);
    clf
    hold on
    for i = 1:numSchemes
        errorbar(deltaVector,meanKErr(i,:,norm),perc*stdKErr(i,:,norm)/sqrt(batchSize),...
            schemeMarkers{i},'MarkerSize',markerSize,'LineWidth',lineWidth);
    end
    hold off

    legend(schemeNames{:},'Location','North')
    title(normNames{norm})
    xlim([min(deltaVector) max(deltaVector)])
    xlabel('$\delta$','Interpreter','Latex')
%     ylabel('Error')
    set(gca,'FontSize',fontSize)
    
    % Trimmed look
    whiteSpaceRemove(gca)
    
    pause(1)
    printToPDF(h,normPlotName{norm},false)
end

%% Vertical AS histogram plot
numBins = 20;
test = permute(kErrorComp,[1 4 3 2]);
yTickVec = [min(test(:)) 0 max(test(:))];
histBins = linspace(yTickVec(1),yTickVec(end),numBins);
yLabVec = {num2str(yTickVec(1),'%10.2f') num2str(yTickVec(2)) num2str(yTickVec(3),'%10.2f')};
deltaStrings = cell(numDelta,1);
deltaDiffs = linspace(-diffSize,diffSize,numDelta);
for p = 1:numDelta
    if(deltaDiffs(p) < 0)
        signString = ' - ';
    else
        signString = ' + ';
    end
    deltaStrings{p} = ['$\hat{\delta}' signString num2str(abs(deltaDiffs(p))) '$'];
end
% Remove the middle index (since that's all zeros)
cutIndex = [1:floor(numDelta/2) floor(numDelta/2)+2:numDelta];
test = test(:,:,cutIndex,:);
deltaStrings = deltaStrings(cutIndex);

vertHistPlotMult(test,schemeNames,histBins,normNames(logScaleNorms),...
    deltaStrings,yTickVec,yLabVec,false)
% Add lines to vertical histograms
pause(1)
addHistoLines(gcf)
pause(1)
printToPDF(gcf,histPlotName,false)