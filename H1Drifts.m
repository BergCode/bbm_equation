% Loads the samples provided by
% errorEstimates.m, and
% errorEstimatesLinear.m
% and plot their H1 drifts together
dataName = cell(2,1);
dataName{1} = 'Data/ErrorEstimates/normEstimates2022Apr'; % Data file name
dataName{2} = 'Data/Linear/normEstimatesLinear2022Apr2'; % Data file name
pStrings = {'$p=1$','$p=0$'};

% Time info
T = 1; % Time horizon
NVec = 2.^[7:16,19]; % Number of time steps
hVec = T./NVec;
numN = length(NVec)-1;

schemeNames = {'SE','LT','St','EE','BE','ME','MP','CN'};
numSchemes = length(schemeNames);
[colMat,schemeMarkers] = SchemePlotInfo(schemeNames);

batchSize = 600;
smallerBatchSize = 50;
numSmallerBatches = batchSize/smallerBatchSize;

%% Rename scheme St to ST
schemeNames{3} = 'ST';

%% Load the data
MaxH1Drift = zeros(batchSize,numN,2,numSchemes); % Extra dimension for later
NIndex = 6;
H1Evol = zeros(batchSize,NVec(NIndex)+1,numSchemes,2);
% Index where the H1 norm is stored
H1Index = 5;

% Load each smaller saple set and load into memory
for expID = 1:2
    for sm = 1:numSmallerBatches
        % Print progress
        fprintf(['-------'...
            '\n Loading smaller batch ', num2str(sm), ...
            ' of ', num2str(numSmallerBatches),...
            '.\n Experiment: ', num2str(expID)...
            '.\n-------\n'])
        load([dataName{expID} '(' num2str(sm) ')']); % Stored in variable
        sampleFloor = (sm-1)*smallerBatchSize;
        
        for m = 1:smallerBatchSize
            for i = 1:numSchemes
                % Extract the drift for the desired time step size
                H1Evol(sampleFloor+m,:,i,expID) = ...
                    variable.normStorage{m,NIndex}(:,i,H1Index);
                for n = 1:numN
                    % Calculate the maximum drift
                    MaxH1Drift(sampleFloor+m,n,expID,i) = ...
                        max(abs(variable.normStorage{m,n}(:,i,H1Index)...
                        -variable.normStorage{m,n}(1,i,H1Index)));
                end
            end
        end
    end
end
H1DriftEvolMeans = squeeze(mean(H1Evol-H1Evol(:,1,:,:)));
clear variable
%% Plot H1 drift
plotName = 'Plots/MaxH1Drift';
% Take at most 4 time steps. It gets too crowded otherwise
NIndexes = [2,6,10];
numUsedN = length(NIndexes);
% Storage needs to have structure (samples,topAxis,rightAxis,schemes)
% We have (batchSize,numN,1,schemes), for optimal vertical space
numBins = 35;
numLabels = 4;
maxVal = ceil(log10(max(MaxH1Drift(:)))); % Will be negative
minVal = max(floor(log10(min(MaxH1Drift(:)))),-17); % Will be negative
yAxisVals = linspace(minVal,maxVal,numBins);
integerVector = minVal:maxVal;
numIntegers = length(integerVector);
yLabelVals = integerVector(floor(linspace(1,numIntegers,numLabels)));
yLabels = cell(numLabels,1);
for i = 1:numLabels
    yLabels{i} = ['$10^{' num2str(yLabelVals(i)) '}$'];
end
topTitle = cell(numUsedN,1);
for n = 1:numUsedN
    topTitle{n} = ['$h=2^{' num2str(log2(hVec(NIndexes(n)))) '}$'];
end
% rightTitle = {''};

vertHistPlotMult(log10(MaxH1Drift(:,NIndexes,:,:)),...
    schemeNames,yAxisVals,topTitle,pStrings,yLabelVals,yLabels)
pause(1)
printToPDF(gcf,plotName,false)
%% Plot H1 evolutions
plotName = 'Plots/Evols/H1Drift';
[fontSize,markerSize,lineWidth] = AMSFontSize;
tVec = linspace(0,T,NVec(NIndex)+1);
% We can't use markers for all time steps, so we make a coarser grid
numMarkers = 2^4;
markerRatio = NVec(NIndex)/numMarkers;
markerTVec = linspace(0,T,numMarkers+1);
markerNIndex = 1:NVec(NIndex);
markerNIndex = markerNIndex(mod(markerNIndex,markerRatio)==0);
markerNIndex = [0 markerNIndex]+1;

titleStrings = {'$p=1$','$p=0$'};

figure(1)
clf
tiledlayout(1,2,"TileSpacing","tight")
for expID = 1:2
    nexttile()
    hold on
    % Dummy line
    for s = 1:numSchemes
        plot([-1, -2],[1 2],['-' schemeMarkers{s}],...
            'Color',colMat(s,:),'MarkerSize',markerSize,'LineWidth',lineWidth)
    end
    % Sparse markers
    for s = 1:numSchemes
    plot(markerTVec,H1DriftEvolMeans(markerNIndex,s,expID),schemeMarkers{s},'Color',colMat(s,:),...
        'MarkerSize',markerSize,'LineWidth',lineWidth)
    end
    % Line
    for s = 1:numSchemes
    plot(tVec,H1DriftEvolMeans(:,s,expID),'-','Color',colMat(s,:),'LineWidth',lineWidth)
    end
    hold off

    % Set the x axis
    xlim([0,T])
    if expID == 1
        legend(schemeNames,'location','southwest')
    end
%     title(titleStrings{expID},'Interpreter','latex')
    xlabel('t')
    if expID==1
        ylabel('$E\left[ ||u_n||_{H^1}-||u_0||_{H^1}\right]$',...
            'Interpreter','latex')
    end
    set(gca,'FontSize',fontSize)
end
pause(0.1)
printToPDF(gcf,plotName,false)

%% Weak convergence H1 norm
plotName = 'Plots/WeakH1Conv';

% Disable title
noTitle = {[],[]};
% Move legend
legPos = 'east';

figure(2)
% titleStrings = {'$p=1$','$p=0$'};
hPows = 1; lineChoice = 3;
meanMaxDrift = squeeze(mean(MaxH1Drift));
meanMaxDrift = permute(meanMaxDrift,[1,3,2]);

plotStrongErrors(meanMaxDrift,hVec,schemeMarkers,colMat,...
    schemeNames,noTitle,hPows,lineChoice,[],noTitle,legPos)
pause(0.1)
printToPDF(gcf,plotName,false)