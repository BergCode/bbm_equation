currFileName = 'errorEstimatesDamp.m';
% [fList,pList] = matlab.codetools.requiredFilesAndProducts('errorEstimates.m');
% folder = fileparts(which(mfilename)); 
% addpath(genpath(folder));
% Modified to use symmetric exponential integrator as reference scheme

% Test sizes
% batchSize = 8;
% smallerBatchSize = 4;
% NVec = 2.^[6:11,13]; % Number of time steps
% M = 2^10; % Number of spatial points / Fourier modes

% OBS: Scheme names are the incorrect order in the specs
% They are changed to refled the correct order in a latter section

batchSize = 600;
smallerBatchSize = 60;
% batchSize = 480;
% smallerBatchSize = 160;
numSmallerBatches = batchSize/smallerBatchSize;
%% !! Experiment Specs - Identifies unique experiment
% Time info
T = 1; % Time horizon
NVec = 2.^[7:14,17]; % Number of time steps

% Spatial info
M = 2^11; % Number of spatial points / Fourier modes
L = 20*pi; % Interval radius
XInt = [-L,L]; % Interval
dx = (XInt(2)-XInt(1))/M; % Spatial grid step size
x = XInt(1) + dx*(0:M-1); % Spatial grid
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2, -M/2+1:-1]; % Fourier modes

% Pre-calc, used in schemes
kSq = absSq(k);
A = -1i*k./(1+kSq);

% Misc. info
IV = @(x) 1.3*exp(-2*x.^2); % Initial value
p = 1; % Nonlinearity power
backupName = 'Data/Dampened/normEstimatesDamp2022Apr'; % Data file name

% Scheme information
schemeNames = {'SE','LT','St','EE','BE','ME','MP','CN'};
numSchemes = length(schemeNames);
[colMat,schemeMarkers] = SchemePlotInfo(schemeNames);

% L2, H1, LInf
normNames = {'L2','H1','LInf','L2','H1','LInf'};
logScaleNorms = [true,true,true,false,false,false];
normFuns = {@(x,y) sqrt(L2norm(x-y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(x-y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(x)-ifft(y))),...
    @(x,y) sqrt(L2norm(y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(y)))};

% !! End of experiment specs
%% Vary the additional terms and run a set of experiments for each variant

% Add a second and zeroeth order term. These will be defined as s and r
% respectively, while being baked into B for the splits and integrators.
% sVec = [1]; rVec = [1]; 
sVec = [-1 0 1]; rVec = [-1 0 1]; 
numS = length(sVec); numR = length(rVec);
% Append to reflect change in s and r
fileNameAppendages = {'sNeg','sNull','sPos';'rNeg','rNull','rPos'}; 

[temp1,temp2] = ndgrid(sVec,rVec); settings = [temp1(:),temp2(:)]; 
[temp1,temp2] = ndgrid(1:numS,1:numR); settingIndexes = [temp1(:),temp2(:)];
numSettings = size(settingIndexes,1);

u0 = fft(IV(x));
% Simulate the samples
for i = 1:numSmallerBatches
    for setting = 1:numSettings
    % Extract parameters
    s = sVec(settingIndexes(setting,1));
    r = sVec(settingIndexes(setting,2));
    % Perform mode-specific pre-calculations
    B = (s*kSq + r)./(1+kSq);
    schemes = {...
        @(currU,h,dW) DampSExp(currU,A,B,dW,h,p),...
        @(currU,h,dW) DampLTSpl(currU,A,B,dW,h,p),...
        @(currU,h,dW) DampStrangSpl(currU,A,B,dW,h,p),...
        @(currU,h,dW) DampExplInt(currU,A,B,dW,h,p),...
        @(currU,h,dW) DampThetaExp(currU,A,B,dW,h,p,0),...
        @(currU,h,dW) DampThetaExp(currU,A,B,dW,h,p,0.5),...
        @(currU,h,dW) DampMPEul(currU,k,kSq,dW,h,p,s,r),...
        @(currU,h,dW) DampCNEul(currU,k,kSq,dW,h,p,s,r)};
    
        indexOffset = (i-1)*smallerBatchSize;
        % Modify file name
        currBackupName = [backupName ...
            fileNameAppendages{1,settingIndexes(setting,1)} ...
            fileNameAppendages{2,settingIndexes(setting,2)} ...
            '(' num2str(i) ')'];
        % Samples are saved, so no need to store them for now
        ErrorCalc(NVec,T,currBackupName,u0,smallerBatchSize,schemes,...
            normFuns,false,indexOffset,currFileName);
    end
end

%% Load available samples and extract, H1 norm drift and convergences
hVec = T./NVec;

numLogPlots = sum(logScaleNorms);
numNorms = length(normFuns);
numN = length(NVec)-1;

% Assuming that it's the first ones that are log scale plots
schemeErrors = zeros(batchSize,numN,numSchemes,numLogPlots,numSettings);
H1Drifts = zeros(batchSize,numN,1,numSchemes,numSettings); % Extra dimension for later

% Load each smaller saple set and load into memory
for setting = 1:numSettings
    for sm = 1:numSmallerBatches
        load([backupName ...
            fileNameAppendages{1,settingIndexes(setting,1)} ...
            fileNameAppendages{2,settingIndexes(setting,2)} ...
            '(' num2str(sm) ')']); % Stored in variable
        sampleFloor = (sm-1)*smallerBatchSize;

        for m = 1:smallerBatchSize
            for n = 1:numN
                for i = 1:numSchemes
                    % Load all the maximum errors
                    for norm = 1:numLogPlots
                        schemeErrors(sampleFloor+m,n,i,norm,setting) = ...
                            max(variable.normStorage{m,n}(:,i,norm)); 
                    end
                    % Load the H1 norm
                    H1Drifts(sampleFloor+m,n,1,i,setting) = ...
                        max(abs(variable.normStorage{m,n}(:,i,5)...
                        -variable.normStorage{m,n}(1,i,5)));
                end
            end
        end
        fprintf(['\nLoaded batch ' num2str(sm) ' of ' num2str(numSmallerBatches)...
            ' and setting ' num2str(setting) ' of ' num2str(numSettings)])
    end
end
fprintf('\n')
clear variable

%% Rename scheme St to ST
schemeNames{3} = 'ST';

%% Quick check & aiming for uniform plot colors and markers
for setting = 1:numSettings
    loglog(hVec(1:end-1),squeeze(mean(schemeErrors(:,:,:,1,setting))))
    pause(2)
end

%% Plot H1 drift
plotName = 'Plots/Evols/H1Drift';
% Take at most 4 time steps. It gets too crowded otherwise
numUsedN = 3;
% Storage needs to have structure (samples,topAxis,rightAxis,schemes)
% We have (batchSize,numN,1,schemes), for optimal vertical space
numBins = 35;
numLabels = 4;
maxVal = ceil(log10(max(H1Drifts(:)))); % Will be negative
minVal = floor(log10(min(H1Drifts(:)))); % Will be negative
yAxisVals = linspace(minVal,maxVal,numBins);
integerVector = minVal:maxVal;
yLabelVals = round(linspace(minVal,maxVal,numLabels));
yLabels = cell(numLabels,1);
for i = 1:numLabels
    yLabels{i} = num2str(yLabelVals(i));
end
topTitle = cell(numUsedN,1);
for n = 1:numUsedN
    topTitle{n} = ['$h=2^{' num2str(log2(hVec(n))) '}$'];
end
rightTitle = {''};

for setting = 1:numSettings
    vertHistPlotMult(log10(H1Drifts(:,1:numUsedN,:,:,setting)),...
        schemeNames,yAxisVals,topTitle,rightTitle,yLabelVals,yLabels)
    pause(1)
    printToPDF(gcf,[plotName...
            fileNameAppendages{1,settingIndexes(setting,1)} ...
            fileNameAppendages{2,settingIndexes(setting,2)}],...
            false) % Don't move to plot folder
end

%% Mean-square convergence
plotName = 'Plots/Strong/Damp/Strong';

% Disable title
noTitle = cell(numLogPlots,1);
for i = 1:numLogPlots
    noTitle{i} = [];
end
% Move legend
legPos = 'southeast';

meanMat = squeeze(mean(schemeErrors));
for setting = 1:numSettings
    figure(setting)
    clf
    hPows = 1; lineChoice = 3;
    plotStrongErrors(meanMat(:,:,:,setting),hVec,schemeMarkers,colMat,...
        schemeNames,noTitle,hPows,lineChoice,[],noTitle,legPos)

    % Save figure
    pause(1)
    printToPDF(gcf,[plotName...
        fileNameAppendages{1,settingIndexes(setting,1)} ...
        fileNameAppendages{2,settingIndexes(setting,2)}],...
        false) % Don't move to plot folder
end
%% Convergence in probability - probability plot
% Use one contant vector per norm
deltaVector = [.9 1 1.1];
fileNames = {'Plots/Prob/Mult/Damp/multProbL2'
    'Plots/Prob/Mult/Damp/multProbH1'
    'Plots/Prob/Mult/Damp/multProbLInf'};
% Choose which scheme which will be used to estimate the C coefficients
% Needs to match the number of settings
schemeChoice = 4*ones(numSettings,1);

for setting = 1:numSettings
    for norm = 1:numLogPlots
        cVec = probCEstimate(schemeErrors(:,:,schemeChoice(setting),norm,setting),...
            deltaVector,hVec(1:end-1));
        probConvMult(schemeErrors(:,:,:,norm,setting),cVec,...
            deltaVector,NVec(1:end-1),hVec,schemeMarkers,colMat)
        % Print to pdf
        pause(1)
        printToPDF(gcf,[fileNames{norm}...
            fileNameAppendages{1,settingIndexes(setting,1)} ...
            fileNameAppendages{2,settingIndexes(setting,2)}],...
            false) % Don't move the plots to the plot folder
        pause(1)
    end
end

%% Convergence in probability - estimate C(epsilon) (alt. 2)
% One plot per scheme and norm
normPlotName = 'Plots/Prob/RangeIntegral/Damp/Prob';
powVec = linspace(0.9,1.1,101);

% Disable title
noTitle = cell(numLogPlots,1);
for i = 1:numLogPlots
    noTitle{i} = [];
end
% Move legend
legPos = 'north';

% Compare with shape
% plot(powVec-1,abs(hVec(1).^(-powVec+1)-hVec(end-1).^(-powVec+1)))
% from lemma
for setting = 1:numSettings
    plotProbConv(schemeErrors(:,:,:,:,setting),hVec,powVec,colMat,...
        noTitle,schemeNames,schemeMarkers,true,legPos)
    
    % Print to pdf
    pause(0.1)
    printToPDF(gcf,[normPlotName ...
        fileNameAppendages{1,settingIndexes(setting,1)} ...
        fileNameAppendages{2,settingIndexes(setting,2)}],false) % Don't move to plot folder
    pause(0.1)
end

%% Almost sure convergence
[fontSize,markerSize,lineWidth] = AMSFontSize;
normPlotName = {'Plots/AS/Damp/ASL2',...
    'Plots/AS/Damp/ASH1',...
    'Plots/AS/Damp/ASInf'};
histPlotName = 'Plots/AS/Damp/Hist';
% Estimate the constant K_\delta(T,\omega) for each sample, and then
% compute the maximum distance to each err_{h}
numDelta = 5;
% Center around the thought convergence
diffSize = 0.5;
deltaVector = linspace(1-diffSize,1+diffSize,numDelta);
% Storage 
kErrorEst = zeros(batchSize,numSchemes,numDelta,numLogPlots);
ratios = zeros(numSchemes,numDelta,numLogPlots,numSettings);

% Assuming that middle index is the sought convergence speed
convSpeedIndex = ceil(numDelta/2);

% Estimate the coefficients and maximum distances to the convergence line
% Calculate the ratio of samples that have a smaller max distance
for setting = 1:numSettings
    for norm = 1:numLogPlots
        % logStepSize = log(dt_num(2))-log(dt_num(1));

        for i = 1:numSchemes
            for j = 1:numDelta
                for m = 1:batchSize
                    % Assuming equally spaced time steps (in log space)
                    % We want the line (in log space) to be normed above the
                    % maximum attained error
                    sampleErrors = log(schemeErrors(m,:,i,norm,setting));
                    lineEst = deltaVector(j)*log(hVec(1:end-1))+...
                        max(sampleErrors-deltaVector(j)*log(hVec(1:end-1)));
                    kErrorEst(m,i,j,norm) = max(abs(sampleErrors-lineEst));
    %                 figure(1)
    %                 plot(log(hVec(1:end-1)),sampleErrors,log(hVec(1:end-1)),lineEst,'r')
    %                 title(['\delta = ' num2str(deltaVector(j))])
    %                 pause(0.1)
                end
            end
            for j = 1:numDelta
                ratios(i,j,norm,setting) = sum(kErrorEst(:,i,j,norm) > ...
                    kErrorEst(:,i,convSpeedIndex,norm))/batchSize;
            end
        end
    end

    % Plot line error for AS conv. 
    kErrorComp = kErrorEst - kErrorEst(:,:,convSpeedIndex,:);
    meanKErr = squeeze(mean(kErrorComp));
    medKErr = squeeze(median(kErrorComp));
    stdKErr = squeeze(std(kErrorComp));

    % Used for CI based on normal distribution
    confLevel = 0.95;
    perc = abs(norminv((1-confLevel)/2,0,1));

    % Plot the errorbars of the difference of the K error
    % Plot histogram of ratios
    for norm = 1:numLogPlots
        h = figure(norm);
        clf
        hold on
        for i = 1:numSchemes
            errorbar(deltaVector,meanKErr(i,:,norm),perc*stdKErr(i,:,norm)/sqrt(batchSize),...
                'Color',colMat(i,:),'Marker',schemeMarkers{i},'MarkerSize',markerSize,'LineWidth',lineWidth);
        end
        hold off
        
        legend(schemeNames{:},'Location','North')
        title(normNames{norm})
        xlim([min(deltaVector) max(deltaVector)])
        xlabel('$\delta$','Interpreter','Latex')
    %     ylabel('Error')
        set(gca,'FontSize',fontSize)

        % Trimmed look
        whiteSpaceRemove(gca)

        pause(1)
        printToPDF(h,[normPlotName{norm}...
                fileNameAppendages{1,settingIndexes(setting,1)} ...
                fileNameAppendages{2,settingIndexes(setting,2)}],...
                false) % Don't move to plot folder
    end
    % Vertical histogram plot
    numBins = 20;
    test = permute(kErrorEst-kErrorEst(:,:,convSpeedIndex,:),[1 4 3 2]);
    yTickVec = [min(test(:)) 0 max(test(:))];
    histBins = linspace(yTickVec(1),yTickVec(end),numBins);
    yLabVec = {num2str(yTickVec(1)) num2str(yTickVec(2)) num2str(yTickVec(3))};
    deltaStrings = cell(numDelta,1);
    deltaDiffs = linspace(-diffSize,diffSize,numDelta);
    for p = 1:numDelta
        if(deltaDiffs(p) < 0)
            signString = ' - ';
        else
            signString = ' + ';
        end
        deltaStrings{p} = ['$\hat{\delta}' signString num2str(abs(deltaDiffs(p))) '$'];
    end

    % Remove the middle index (since that's all zeros)
    cutIndex = [1:floor(numDelta/2) floor(numDelta/2)+2:numDelta];
    test = test(:,:,cutIndex,:);
    deltaStrings = deltaStrings(cutIndex);

    vertHistPlotMult(test,schemeNames,histBins,normNames(logScaleNorms),...
        deltaStrings,yTickVec,yLabVec,false)
    pause(0.5)
    % Add lines to vertical histograms
    addHistoLines(gcf)
    pause(0.5)
    printToPDF(gcf,[histPlotName ...
            fileNameAppendages{1,settingIndexes(setting,1)} ...
            fileNameAppendages{2,settingIndexes(setting,2)}],...
            false) % Don't move to plot folder
end