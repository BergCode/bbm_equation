currFileName = 'errorEstimatesLinear.m';
% [fList,pList] = matlab.codetools.requiredFilesAndProducts('errorEstimates.m');

batchSize = 600;
smallerBatchSize = 50;
numSmallerBatches = batchSize/smallerBatchSize;

%% !! Experiment Specs - Identifies unique experiment
% Time info
T = 1; % Time horizon
NVec = 2.^[7:16,19]; % Number of time steps

% Spatial info
M = 2^12; % Number of spatial points / Fourier modes
L = 20*pi; % Interval radius
XInt = [-L,L]; % Interval
dx = (XInt(2)-XInt(1))/M; % Spatial grid step size
x = XInt(1) + dx*(0:M-1); % Spatial grid
k = 2*pi/(XInt(2)-XInt(1))*[0:M/2, -M/2+1:-1]; % Fourier modes

% Pre-calc, used in schemes
kSq = absSq(k);
A = -1i*k./(1+kSq);

% Misc. info
IV = @(x) 1.3*exp(-2*x.^2); % Initial value
p = 0; % Nonlinearity power
backupName = 'Data/Linear/normEstimatesLinear2022Apr2'; % Data file name

schemes = {...
    @(currU,h,dW) SExp(currU,A,dW,h,p),...
    @(currU,h,dW) LTSpl(currU,A,dW,h,p),...
    @(currU,h,dW) StrangSpl(currU,A,dW,h,p),...
    @(currU,h,dW) explInt(currU,A,dW,h,p),...
    @(currU,h,dW) thetaExp(currU,A,dW,h,p,0),...
    @(currU,h,dW) thetaExp(currU,A,dW,h,p,0.5),...
    @(currU,h,dW) MPEul(currU,k,kSq,dW,h,p),...
    @(currU,h,dW) CNEul(currU,k,kSq,dW,h,p)};
numSchemes = length(schemes);
schemeNames = {'SE','LT','St','EE','BE','ME','MP','CN'};
[colMat,schemeMarkers] = SchemePlotInfo(schemeNames);

% L2, H1, LInf
normNames = {'L2','H1','LInf','L2','H1','LInf'};
logScaleNorms = [true,true,true,false,false,false];
normFuns = {@(x,y) sqrt(L2norm(x-y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(x-y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(x)-ifft(y))),...
    @(x,y) sqrt(L2norm(y,dx,true,false,L)),...
    @(x,y) sqrt(H1norm(y,dx,k,true,false,L)),...
    @(x,y) max(abs(ifft(y)))};

% !! End of experiment specs
%% Simulate experiment
u0 = fft(IV(x));
for i = 1:numSmallerBatches
    indexOffset = (i-1)*smallerBatchSize;
    currBackupName = [backupName '(' num2str(i) ')'];
    % Samples are saved, so no need to store them for now
    ErrorCalc(NVec,T,currBackupName,u0,smallerBatchSize,schemes,...
        normFuns,false,indexOffset,currFileName);
end
%% Load available samples and extract, H1 norm drift and convergences
hVec = T./NVec;

numLogPlots = sum(logScaleNorms);
numNorms = length(normFuns);
numN = length(NVec)-1;

% Assuming that it's the first ones that are log scale plots
schemeErrors = zeros(batchSize,numN,numSchemes,numLogPlots);

% Load each smaller saple set and load into memory
for sm = 1:numSmallerBatches
    load([backupName '(' num2str(sm) ')']); % Stored in variable
    sampleFloor = (sm-1)*smallerBatchSize;
    
    for m = 1:smallerBatchSize
        for n = 1:numN
            for i = 1:numSchemes
                % Load all the maximum errors
                for norm = 1:numLogPlots
                    schemeErrors(sampleFloor+m,n,i,norm) = ...
                        max(variable.normStorage{m,n}(:,i,norm)); 
                end
            end
        end
    end
    fprintf(['\nLoaded batch ' num2str(sm) ' of ' num2str(numSmallerBatches)])
end
fprintf('\n')
clear variable

%% Rename scheme St to ST
schemeNames{3} = 'ST';

%% Mean-square convergence
plotName = 'Plots/Strong/StrongLinear';

% Disable title
noTitle = cell(numLogPlots,1);
for i = 1:numLogPlots
    noTitle{i} = [];
end
% Move legend
legPos = 'southeast';

figure(1)
hPows = [1,2];
lineChoice = [3,1];
meanMat = squeeze(mean(schemeErrors));
plotStrongErrors(meanMat,hVec,schemeMarkers,colMat,...
    schemeNames,normNames,hPows,lineChoice,[],noTitle,legPos)
% Save figure
pause(1)
printToPDF(gcf,plotName,false)
%% Convergence in probability - probability plot
% We need to split the schemes into three groups for this one
% SE alone, Euler schemes together, and splitting schemes and exponential
% integrators together.
numSplits = 3;
schemeSplit = {false(numSchemes,1),false(numSchemes,1),false(numSchemes,1)};
    schemeSplit{1}(1:1) = true(1,1); % Enable SE
    schemeSplit{2}(2:6) = true(5,1); % Enable splitting and exponential integrators
    schemeSplit{3}(7:8) = true(2,1); % Enable MP and CN
% Choose which scheme which will be used to estimate the C coefficients
% Needs to match the number of splits
schemeChoice = ones(numSplits,1);
% Use one contant vector per norm
deltaVectors = {[1.9 2 2.1],[.9 1 1.1]};
% Name info
nameStart = 'Plots/Prob/Mult/Linear/multProbLinear';
normAppend = normNames(logScaleNorms);
splitNames = {'SE','SplExp','Euls'};
for spl = 1:numSplits
    for norm = 1:numLogPlots
        if spl == 1
            deltaVector = deltaVectors{1};
        else
            deltaVector = deltaVectors{2};
        end
        tempErrors = schemeErrors(:,:,schemeSplit{spl},norm);
        % Estimate which C valures are best to use in this case
        cVec = probCEstimate(tempErrors,deltaVector,hVec(1:end-1));
        probConvMult(tempErrors,cVec,...
            deltaVector,NVec(1:end-1),hVec,schemeMarkers(schemeSplit{spl}),...
            colMat(schemeSplit{spl},:))
        % Print to pdf
        pause(1)
        printToPDF(gcf,[nameStart normAppend{norm} splitNames{spl}],false)
    end
end

%% Convergence in probability - estimate C(epsilon) (alt. 2)
plotNameBase = 'Plots/Prob/RangeIntegral/ProbLin';
powVecs = cell(2,1);
    powVecs{1} = linspace(0.9,1.1,101);
    powVecs{2} = linspace(1.9,2.1,101); % For SE
schemeIndexes = cell(2,1);
    schemeIndexes{1} = true(numSchemes,1);
    schemeIndexes{1}(1) = false;
    schemeIndexes{2} = false(numSchemes,1);
    schemeIndexes{2}(1) = true;

% Disable title
noTitle = cell(numLogPlots,1);
for i = 1:numLogPlots
    noTitle{i} = [];
end
% Move legend
legPos = 'southeast';

% Compare with shape
% plot(powVec-1,abs(hVec(1).^(-powVec+1)-hVec(end-1).^(-powVec+1)))
% from lemma
for i = 1:2
    if i==1
        plotName = plotNameBase;
    else
        plotName = [plotNameBase schemeNames{1}];
    end
    powVec = powVecs{i};
    schemeIndex = schemeIndexes{i};

    plotProbConv(schemeErrors(:,:,schemeIndex,:),hVec,powVec,...
        colMat(schemeIndex,:),noTitle,schemeNames(schemeIndex),...
        schemeMarkers(schemeIndex),true,legPos)
    % Print to pdf
    pause(0.1)
    printToPDF(gcf,plotName,false) % Don't move to plot folder
    pause(0.1)
end


%% Almost sure convergence
% We need to split the schemes into three groups for this one
% SE alone, Euler schemes together, and splitting schemes and exponential
% integrators together.
numSplits = 3;
schemeSplit = {false(numSchemes,1),false(numSchemes,1),false(numSchemes,1)};
    schemeSplit{1}(1:1) = true(1,1); % Enable SE
    schemeSplit{2}(2:6) = true(5,1); % Enable splitting and exponential integrators
    schemeSplit{3}(7:8) = true(2,1); % Enable MP and CN
splitIndexes = [1, 2*ones(1,7)];
    
plotFolder = 'Plots/AS/Linear/';
normAppend = normNames(logScaleNorms);
splitNames = {'SE','SplExp','Euls'};
% Estimate the constant K_\delta(T,\omega) for each sample, and then
% compute the maximum distance to each err_{h}
numDelta = 5;
% Center around the thought convergence
diffSize = 0.2;
delta1Vec = linspace(1-diffSize,1+diffSize,numDelta);
delta2Vec = linspace(2-diffSize,2+diffSize,numDelta);
deltaVectors = { delta2Vec ; delta1Vec};
kErrorEst = zeros(batchSize,numSchemes,numDelta,numLogPlots,2);

for norm = 1:numLogPlots
    for i = 1:numSchemes
        deltaVector = deltaVectors{splitIndexes(i)};
        for m = 1:batchSize
                for j = 1:numDelta
                    % Alternative 1:
                    % Assuming equally spaced time steps (in log space)
                    % We want the line (in log space) to be normed above the
                    % maximum attained error
                    hLogVals = log(hVec(1:end-1));
                    errorLogVals = log(schemeErrors(m,:,i,norm));
                    maxLineEst = deltaVector(j)*hLogVals+...
                        max(errorLogVals-deltaVector(j)*hLogVals);
                    kErrorEst(m,i,j,norm,1) = max(abs(errorLogVals-maxLineEst));
                    
                    % !!!! Accomodate the changes!
                    % Alternative 2:
                    % Make a least-squares fit of a line for the
                    % initial guess on the translation coefficent
                    polCoeff = polyfit(hLogVals,errorLogVals,1); 
                    polCoeff(1) = deltaVector(j); % Fix first term (slope)

                    % We estimate the constrained (fixed line slope)
                    % polynomial translation using fminsearch
                    polyFunc = @(transl) polyval([deltaVector(j) transl],hLogVals); 
                    objFunc = @(transl) sum((errorLogVals - polyFunc(transl)).^2);
                    initGuess = polCoeff(2);
                    polCoeff(2) = fminsearch(objFunc,initGuess);

                    % Calculate error between true error and line est
                    polLineEst = polyval(polCoeff,hLogVals);
                    kErrorEst(m,i,j,norm,2) = max(abs(errorLogVals-polLineEst));

%                     figure(1)
%                     clf
%                     hold on
%                     plot(log(hVec(1:end-1)),errorLogVals,'g')
%                     plot(log(hVec(1:end-1)),maxLineEst,'r')
%                     plot(log(hVec(1:end-1)),polLineEst,'k')
%                     hold off
%                     title(['\delta = ' num2str(deltaVector(j))])
%                     legend('True','MaxEst','PolEst')
%                     pause(0.1)
                end
%                 pause(2)
        end
    end
end
%% Plot line error for AS conv. 
numSplits = 2;
schemeSplit = {false(numSchemes,1),false(numSchemes,1)};
    schemeSplit{1}(1:1) = true(1,1); % Enable SE
    schemeSplit{2}(2:8) = true(7,1); % Enable everything else
splitNames = {'SE','Rems'};
% Line estimation method: 1 for line on top, 2 for least square
[fontSize,markerSize,lineWidth] = AMSFontSize;
lineEstIndex = 1;
% Assuming that middle index is the sought convergence speed
convSpeedIndex = ceil(numDelta/2);
kErrorComp = kErrorEst - kErrorEst(:,:,convSpeedIndex,:,:);
meanKErr = squeeze(mean(kErrorComp));
medKErr = squeeze(median(kErrorComp));
stdKErr = squeeze(std(kErrorComp));

% Used for CI based on normal distribution
confLevel = 0.95;
perc = abs(norminv((1-confLevel)/2,0,1));

for spl = 1:numSplits
    for norm = 1:numLogPlots
        h = figure(norm);
        clf
        hold on
        schemeToPlot = 1:numSchemes;
        schemeToPlot = schemeToPlot(schemeSplit{spl});
        for i = schemeToPlot
            deltaVector = deltaVectors{splitIndexes(i)};
            errorbar(deltaVector,meanKErr(i,:,norm,lineEstIndex),...
                perc*stdKErr(i,:,norm,lineEstIndex)/sqrt(batchSize),...
                schemeMarkers{i},'MarkerSize',markerSize,'LineWidth',lineWidth);
        end
        hold off
        legend(schemeNames{schemeSplit{spl}},'Location','North')
        title(normNames{norm})
        xlim([min(deltaVector) max(deltaVector)])
        xlabel('$\delta$','Interpreter','Latex')
    %     ylabel('Error')
        set(gca,'FontSize',fontSize)
        
        % Trimmed look
        whiteSpaceRemove(gca)
        
        pause(1)
        printToPDF(h,[plotFolder 'AS' normAppend{norm} splitNames{spl}],false)
        pause(1)
    end
end
%% Vertical AS histogram plot
numBins = 20;
test = permute(kErrorComp(:,:,:,:,lineEstIndex),[1 4 3 2]);
yTickVec = [min(test(:)) 0 max(test(:))];
histBins = linspace(yTickVec(1),yTickVec(end),numBins);
yLabVec = {num2str(yTickVec(1),'%10.2f') num2str(yTickVec(2)) num2str(yTickVec(3),'%10.2f')};
deltaStrings = cell(numDelta,1);
deltaDiffs = linspace(-diffSize,diffSize,numDelta);
for p = 1:numDelta
    if(deltaDiffs(p) < 0)
        signString = ' - ';
    else
        signString = ' + ';
    end
    deltaStrings{p} = ['$\hat{\delta}' signString num2str(abs(deltaDiffs(p))) '$'];
end
% Remove the middle index (since that's all zeros)
cutIndex = [1:floor(numDelta/2) floor(numDelta/2)+2:numDelta];
test = test(:,:,cutIndex,:);
deltaStrings = deltaStrings(cutIndex);

vertHistPlotMult(test,schemeNames,histBins,normNames(logScaleNorms),...
    deltaStrings,yTickVec,yLabVec,false)
% Add lines to vertical histograms
pause(1)
addHistoLines(gcf)
pause(1)
printToPDF(gcf,[plotFolder 'Hist'],false)
%% Functions used for simplicity (Conv prob version 2)
function locString = legendLoc(sch)
    if sch < 5
            locString = 'SouthEast';
    else
        locString = 'NorthWest';
    end
end