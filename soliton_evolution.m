% Collision of 2 solitons
currFileName = 'soliton_evolution.m';
% [fList,pList] = matlab.codetools.requiredFilesAndProducts(currFileName);

%% !! Experiment Specs - Identifies unique experiment
% Time info
T = 40; % Time horizon
N = 2^13; % Number of time steps
h = T/N;

% Spatial info
M = 2^15; % Number of spatial points / Fourier modes
L = 250; % Interval radius
x_int = [-L,L]; % Interval
dx = (x_int(2)-x_int(1))/M; % Spatial grid step size
x = x_int(1) + dx*(0:M-1); % Spatial grid
k = 2*pi/(x_int(2)-x_int(1))*[0:M/2, -M/2+1:-1]; % Fourier modes

% Pre-calc, used in schemes
k_sq = absSq(k);
A = -1i*k./(1+k_sq);

% Soliton info - Initial value
p = 1; % Nonlinearity power
% p_vec = [1,2,3]; % Nonlinearity powers
c_vec = [2,5];
x_0_vec = [-50,-100];
IV = @(x,c,x_0) 3*(c-1)*sech(.5*sqrt(1-1/c)*(x-x_0)).^2; % Initial value - Soliton
% backupName = 'Data/ErrorEstimates/normEstimates2022Apr'; % Data file name


% Scheme info
schemes = { @(currU) DetSExp(currU,A,A,h,p),...
            @(currU) DetExplInt(currU,A,A,h,p),...
            @(currU) DetMPEul(currU,k,k_sq,h,p,0,1,0)};
num_schemes = length(schemes);
scheme_names = {'SE','EE','MP'};
[col_mat,scheme_markers] = SchemePlotInfo(scheme_names);

% !! End of experiment specs
%% Query storage
% Query fidelity
time_fidelity = 2^10;
space_fidelity = 2^12;

if N > time_fidelity
    time_scale_factor = N / time_fidelity;
    t_indexes = 0:time_scale_factor:N;
else
    time_scale_factor = 1;
    t_indexes = 0:N;
end
if M > space_fidelity
    space_scale_factor = M / space_fidelity;
    space_indexes = 1:space_scale_factor:M;
else
    space_indexes = 1:M;
end
u_storage = zeros(length(t_indexes),length(space_indexes),num_schemes);
query_index = 2;

%% Simulate experiment
% Create initial value
u_0 = zeros(1,M);
max_height = 0;
for i = 1:length(c_vec)
    u_0 = u_0 + IV(x,c_vec(i),x_0_vec(i));
    max_height = max(max_height,3*(c_vec(i)-1));
end
orig_u_0 = u_0;
u_0 = fft(u_0);
curr_u = cell(num_schemes,1);
for sch = 1:num_schemes
    curr_u{sch} = u_0;
    u_storage(1,:,sch) = orig_u_0(space_indexes);
end

% Plot only a fraction of the steps
plot_run = false;
num_plots = 2^7;
plot_frac = N/num_plots;

figure(1)
clf
tiledlayout(1,3,"TileSpacing","compact","Padding","compact")
for i = 1:N
    for sch = 1:num_schemes
        curr_u{sch} = schemes{sch}(curr_u{sch});
        % Store the solution if at the correct time step
        if mod(i,time_scale_factor) == 0
            temp_curr_u = ifft(curr_u{sch});
            u_storage(query_index+1,:,sch) = temp_curr_u(space_indexes);
            % Only progress the query index at the last scheme
            if sch == num_schemes
                query_index = query_index+1;
                disp(['Fraction of time ' num2str(i/N)])
            end
        end
        % Plot if we want to observe the trajectories
        if plot_run && mod(i,plot_frac) == 1
            nexttile(sch)
            plot(x,real(ifft(curr_u{sch})))
            ylim([-2,max_height])
            pause(0.01)
        end
    end
end
disp('Done')

%% Plot endpoint
figure(1)
clf
tiledlayout(1,3,"TileSpacing","compact","Padding","compact")
min_point = 0;
for sch = 1:num_schemes
%     min(ifft(curr_u{sch}))
    min_point = min(min_point,min(real(ifft(curr_u{sch}))));
end
for sch = 1:num_schemes
    nexttile(sch)
%     plot(x,real(ifft(curr_u{sch})))
    plot(x,abs(real(ifft(curr_u{sch}))))
    set(gca,'YScale','log')
%     ylim([min_point,0.03])
end

%% Plot evolution
plot_name = 'Plots/Deterministic/Soliton/Evol_PS_';
size_mod = {'big_', 'small_'};
t_vec = linspace(0,T,time_fidelity+2);
x_vec = x(space_indexes);

curr_fig = 1;
for sch = 1:num_schemes
    temp_evol = u_storage(:,:,sch);
    scheme_max_val = max(real(temp_evol(:)));
    scheme_min_val = min(real(temp_evol(:)));
    % Plot big and small movements
    for i = 1:2
        % Export the plotting to make it more readable
        figure(curr_fig)
        curr_fig = curr_fig+1;
        clf

        if i == 1
            z_lim = [scheme_min_val,scheme_max_val];
        else
            z_lim = [scheme_min_val,0.03];
        end

        fig_uniform(x_vec,t_vec,real(u_storage(:,:,sch)),z_lim)
        xlim([-120,175])


        % Export image with transparent background
        % Get a snapshot of current look
        pause(1)
        F = getframe(gcf);
        % Export to a separate figure
        figure(num_schemes+1)
        imshow(F.cdata);
        
        I = getimage(gca);
        imwrite(I,[plot_name size_mod{i} scheme_names{sch} '.png'])
        pause(1)
    end

end

%% Support functions
function fig_uniform(x,y,z,z_lim)
    [font_size,marker_size,line_width] = AMSFontSize;

    % Reduce padding
    tiledlayout(1,1,'Padding','compact')
    nexttile

    % Produce the surfaces
    surf(x,y,z,'LineStyle','none')
    colorbar

    % Adapt z axis
    zlim(z_lim)
    caxis(z_lim)

    % Set standard sizes
    set(gca,'LineWidth',line_width)
    set(gca,'FontSize',font_size)

    % Set background color
    set(gcf, 'color', 'w');

    % Set labels
    ylabel('$t$','Interpreter','latex');
    xlabel('$x$','Interpreter','latex');

    % Set camera position to top-down perspective
    view(2)

    % Enforce maximum figure size for highest possible resolution
    set(gcf,'position',[0 0 1 1],'units','normalized')
    pause(1)
end