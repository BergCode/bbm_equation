% Collision of 2 solitons with opposite amplitudes
currFileName = 'soliton_det_and_stoch.m';
% [fList,pList] = matlab.codetools.requiredFilesAndProducts(currFileName);

%% !! Experiment Specs - Identifies unique experiment
batch_size = 1000;
% Time info
T = 10; % Time horizon
N = 2^11; % Number of time steps
h = T/N;

% Spatial info
M = 2^11; % Number of spatial points / Fourier modes
L = 200; % Interval radius
x_int = [-L,L]; % Interval
int_width = x_int(2) - x_int(1);
dx = (x_int(2)-x_int(1))/M; % Spatial grid step size
x = x_int(1) + dx*(0:M-1); % Spatial grid
k = 2*pi/(x_int(2)-x_int(1))*[0:M/2, -M/2+1:-1]; % Fourier modes

% Pre-calc, used in schemes
k_sq = absSq(k);
A = -1i*k./(1+k_sq);

% Soliton info - Initial value
p = 1; % Nonlinearity power
c_vec = [-2, 3, 7];
x_0 = 0;
IV = @(x,c,x_0) 3*(c-1)*sech(.5*sqrt(1-1/c)*(x-x_0)).^2; % Initial value - Soliton
num_IV = length(c_vec);

% Scheme info
scheme_det = @(currU,dW) DetSExp(currU,A,A,h,p);
scheme_EWE_det = @(currU,dW) DetSExp(currU,A,temp_zero,h,p);
scheme_stoch = @(currU,dW) SExp(currU,A,dW,h,p);

% The number of stochastic settings 
% Deterministic, and the following noise coefficients
gamma_vec = [1,1/10];
gamma_label = {'1','0.1'};
gamma_colors = {'b','m'};
gamma_markers = {'s','d'};
num_stoch_settings = length(gamma_vec);
legend_label = [{'Det'},gamma_label(:)'];

% !! End of experiment specs
%% Query storage
% Define the functions that we want to see the drift of
energy = @(x) trapz(x.^3+3*x.^2)*dx;
H1_fun = @(x) H1norm(x,dx,k,true,false,int_width);
mass_fun = @(x) trapz(wrapIfft(x))*dx;
energy_fun = @(x) energy(wrapIfft(x));

x_ext = [x,x(1)];
mass_center_fun = @(x) trapz(wrapIfft(x).*x_ext)*dx/mass_fun(x);
pulse_width_fun = @(x) trapz(wrapIfft(x).*absSq(x_ext-mass_center_fun(x)))*dx/mass_fun(x);

evol_funs = {H1_fun,energy_fun,mass_center_fun,pulse_width_fun};
num_funs = length(evol_funs);
fun_names = {'H1','Energy','Center','Viriel'};

% Initialize the drift storage
evol_storage = zeros(batch_size,N+1,num_funs,num_IV,num_stoch_settings);

%% Create initial values
curr_u = cell(num_IV,num_stoch_settings);

for i = 1:num_IV
    u_0 = fft(IV(x,c_vec(i),x_0));
    % Set initial values
    for j = 1:num_stoch_settings
        curr_u{i,j} = u_0;
    end
end

%% Simulate evolution
% Run deterministic simulation of BBM
evol_storage_det = sample_simulation(1,scheme_det,curr_u(:,1),N,h,1,evol_funs);
% Run deterministic simulation of EWE
evol_storage_EWE_det = sample_simulation(1,scheme_EWE_det,curr_u(:,1),N,h,1,evol_funs);
% Run batch of stochastic simulation
parfor m = 1:batch_size
    evol_storage(m,:,:,:,:) = sample_simulation(m,scheme_stoch,curr_u,N,h,gamma_vec,evol_funs);
end

%% Save data
save('Data/Soliton_det_and_stoch.mat',"evol_storage")

%% Plot evolutions
plot_name = 'Plots/Solitons/Soliton_property_';
% Misc. treatment
[font_size,marker_size,line_width] = AMSFontSize;
t_vec = linspace(0,T,N+1);
mean_mat = squeeze(mean(evol_storage));
% Sparse markers
num_markers = 16;
marker_indexes = linspace(1,N+1,num_markers+1);

for i = 1:num_funs
    figure(i)
    clf
    tiledlayout(num_IV,1,'TileSpacing','tight','Padding','compact')
    for j = 1:num_IV
        nexttile()
        % Plot deterministic
        plot(t_vec,real(evol_storage_det(:,i,j)),'Color','r','LineWidth',line_width)
        hold on
        plot(t_vec,real(evol_storage_EWE_det(:,i,j)),'Color','k','LineWidth',line_width)
        % Plot stochastic
        for k = 1:num_stoch_settings
            plot(t_vec,real(squeeze(mean_mat(:,i,j,k))),...
                'Color',gamma_colors{k},'LineWidth',line_width)
        end
%         legend(legend_label,'Location','northwest')
        % Plot sparse markers
        plot(t_vec(marker_indexes),real(evol_storage_det(marker_indexes,i,j)),...
            'Color','r','Marker','pentagram','LineStyle','none','MarkerSize',marker_size)
        plot(t_vec(marker_indexes),real(evol_storage_EWE_det(marker_indexes,i,j)),...
            'Color','k','Marker','*','LineStyle','none','MarkerSize',marker_size)
        for k = 1:num_stoch_settings
            plot(t_vec(marker_indexes),real(squeeze(mean_mat(marker_indexes,i,j,k))),...
                'Color',gamma_colors{k},'Marker',gamma_markers{k},...
                'LineStyle','none','MarkerSize',marker_size)
        end
        set(gca,'FontSize',font_size)

        % Mark time only at bottom
        if j == num_IV
            xlabel('$t$','Interpreter','latex')
        end
    end

    pause(1)
    printToPDF(gcf,[plot_name fun_names{i}],false)
%     printToPDF(gcf,[plot_name fun_names{i}],false,[1/2,1/2])
end

%% Support functions
function fun_vals = calc_funs(fun_cell,curr_u)
    % Initialize storage
    num_funs = length(fun_cell);
    num_IV = size(curr_u,1);
    num_settings = size(curr_u,2);
    fun_vals = zeros(num_funs,num_IV,num_settings);

    for i = 1:num_funs
        for j = 1:num_IV
            for k = 1:num_settings
                fun_vals(i,j,k) = fun_cell{i}(curr_u{j,k});
            end
        end
    end
end

function drift_storage = sample_simulation(sample_id,scheme,curr_u,N,h,gamma_vec,evol_funs)
% Simulate the stochastic samples and calculate the functions
    num_stoch_settings = length(gamma_vec);
    num_IV = size(curr_u,1);
    num_funs = length(evol_funs);

    % Brownian motion
    rng(sample_id,'twister')
    dW = randn(N,2)*sqrt(h/2);
    
    drift_storage = zeros(N+1,num_funs,num_IV,num_stoch_settings);
    % Set first function values
    for i = 1:num_IV
        for j = 1:num_funs
            drift_storage(1,j,i,:) = evol_funs{j}(curr_u{i,1});
        end
    end

    % Simulate process
    for i = 1:N
        for j = 1:num_IV
            % Stochastic
            for k = 1:num_stoch_settings
                curr_dW = dW(i,:)*gamma_vec(k);
                curr_u{j,k} = scheme(curr_u{j,k},curr_dW);
            end
        end
        drift_storage(i+1,:,:,:) = calc_funs(evol_funs,curr_u);
        if mod(i,N/16)==0
            disp(['Fraction done: ' num2str(i/N) ' of sample ID ' num2str(sample_id)])
        end
    end
    disp('Done')
end